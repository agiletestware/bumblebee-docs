title: IBM UrbanCode and HP ALM Integration Guide - Bumblebee Documentation
description: Bumblebee is a plugin for IBM UrbanCode that allows users to integrate any testing framework with HP ALM without any code changes or custom tools

# IBM UrbanCode + HP ALM Integration Guide

Integrating IBM UrbanCode with HP ALM can be easily achieved using the Bumblebee plugin for IBM UrbanCode. Once configured, IBM UrbanCode can run tests in HP Performance Center

## - Prerequisites

*	Bumblebee Server must be installed and reachable from IBM UrbanCode agent host.

## - Install plugin

*	Download plugin zip file from [Agiletestware website](https://www.agiletestware.com/bumblebee#download)
*	Open IBM UrbanCode Deploy GUI and navigate to **"Settings"** -> **"Automation Plugins"**

[![Plugin installation](../img/ci_integration/ucd-install-1.png)](../img/ci_integration/ucd-install-1.png)

*	Click on **"Load Plugin"** button

[![Plugin installation](../img/ci_integration/ucd-install-2.png)](../img/ci_integration/ucd-install-2.png)

*	Click on **"Browse"** button and select plugin zip file
*	Click on **"Submit"** button

[![Plugin installation](../img/ci_integration/ucd-install-3.png)](../img/ci_integration/ucd-install-3.png)


## - Running HP Performance Center tests from IBM UrbanCode Deploy

### Configuration of "Start Performance Center test" step

After installing the plugin, a new **"Bumblebee\Start Performance Center test"** is available for users on process design screen:

[![Step configuration](../img/ci_integration/ucd-pc_config-1.png)](../img/ci_integration/ucd-pc_config-1.png)

[![Step configuration](../img/ci_integration/ucd-pc_config-2.png)](../img/ci_integration/ucd-pc_config-2.png)

The following configuration properties are available for the step:

| Parameter name    | Description                                        |
|-------------------|----------------------------------------------------|
| Bumblebee URL     | URL for Bumblebee server, e.g. http://servername:8080/bumblebee	|
| HP ALM URL     	| URL for HP ALM server, e.g. http://almservername:8080/qcbin	|
| HP PC URL     	| URL for HP Performance Center server, e.g. http://pcservername:8080	|
| HP ALM user name  | Name of a user in HP ALM/PC	|
| HP ALM password	| Password for a user in HP ALM/PC	|
| Domain            | Domain name in HP ALM                              |
| Project           | Project name in HP ALM                             |
| Results Directory | Directory to which test result files will be saved |
| Path To Test      | Path to a test in HP ALM TestPlan, e.g. "Subject\folder1\test", where "Subject\folder" is a path to a test folder and "test" is the name of a test to run|
| Test Set			| Path to a test set in HP ALM TestLab, containing correspondent test instance, e.g. "Root\folder1\testSet", where "Root\folder1" is a path to a test lab folder and "testSet" is the name of a test set. If test set does not exist or test is not assigned to it, Bumblebee task will try to create a new test set and assign a test to it.|
| Post Run Action	| Defines what PC should do after a test run. Available options: Collate And Analyze, Collate Results and Do Not Collate.|
| Time Slot Duration| Time to allot for the test (PC parameter). It cannot be less than 30 minutes (limitation by PC).|
| Use VUD Licenses	| If true, the test consumes Virtual User Day (VUD) licenses.|
| Timeout			| overrides a global PC timeout value and represents the number of minutes to wait for the Performance Center test to finish. 0 means wait indefinitely.|
| Retry Attempts	| Number of retry attempts, before task completely fails.|
| Retry Interval	| Number of seconds to wait between retry attempts.|
| Interval Increase Factor| Increase factor for retry interval. E.g. if it is set to 2, then each subsequent wait interval between attempts will be twice bigger than the previous one.|
| Collate/Analyze retry attempts	| Number of retry attempts for Collate/Analyze phases, before task completely fails.|
| Collate/Analyze retry interval	| Number of seconds to wait between retry attempts.|
| Polling Interval	| The number of minutes between two test state requests.|
| Fail Build If Task Fails | If true and task has failed (or timeout has reached), then the whole build will be failed. If false, then build will not be failed even if task has failed.|

[![Step configuration](../img/ci_integration/ucd-pc_config-3.png)](../img/ci_integration/ucd-pc_config-3.png)

### Running "Start Performance Center test" step

Here is a sample output for "Start Performance Center test" step:

[![Run Results](../img/ci_integration/ucd-pc_run-1.png)](../img/ci_integration/ucd-pc_run-1.png)
