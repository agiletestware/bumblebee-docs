title: General Troubleshooting - Bumblebee Documentation
description: General Troubleshooting guide 

**Bumblebee Server Logs**

Any time you think there is some Bumblebee issue, you should check out the server logs possibles details. Server logs can be accessed via ==http://yourserver:port/bumblebee/log== . If you can't figure out the issue, submit your issue via [contact-us page ](https://www.agiletestware.com/contact-us "contact-us"). Include the issue description, and attached the ==bumblebee server logs==
