title: Appium and HP ALM Integration Guide - Bumblebee Documentation
description: In this guide, we will demonstrate how Appium tests can be easily integrated with HP ALM using Bumblebee.

# Appium and HP ALM Testing Tool Integration Guide

[Appium](http://appium.io/) is a popular open-source test automation tool for mobile applications.  

It allows users to automate testing of their Android/iOS/Windows mobile applications in platform-independent way, so many companies use it for testing.  

On the other hand, many companies use HP ALM for tracking purposes, but Appium does not have any integration with HP ALM, so it requires much time and efforts to implement such kind of integration.  

Agiletestware Bumblebee can help to solve this problem by introducing Java annotations package which allows users to simply push their test results into HP ALM.  

In this guide, we will show how easy Appium Android tests can be integrated with HP ALM using Bumblebee.

## Prerequisites
To use Bumblebee annotations package with Appium tests, the following software products are needed:

*	[Java Development Kit](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
*   Bumblebee Server ([installation instructions](../setup/server-installation.md))
*   [Appium Desktop](https://github.com/appium/appium-desktop/releases/latest)
*	[Android Studio and SDK](https://developer.android.com/studio/index.html)
*	[Apache Maven](https://maven.apache.org/)
*	[HP ALM](https://saas.hpe.com/en-us/software/application-lifecycle-management)

---

## Creating simple Android project
To create simple Android project, just follow the steps below:

*	Open Andriod Studio and select "**Start a new Android Studio project**"

[![](../img/qa-frameworks/appium-android-1.png)](../img/qa-frameworks/appium-android-1.png)

*	Fill in all fields of "Create New Project" dialog and click **Next**

[![](../img/qa-frameworks/appium-android-2.png)](../img/qa-frameworks/appium-android-2.png)

*	Select "Minimum SDK" and click **Next**

[![](../img/qa-frameworks/appium-android-3.png)](../img/qa-frameworks/appium-android-3.png)

*	Select "Basic Activity" and click **Next**

[![](../img/qa-frameworks/appium-android-4.png)](../img/qa-frameworks/appium-android-4.png)

*	Set activity name and title and click **Finish**

[![](../img/qa-frameworks/appium-android-5.png)](../img/qa-frameworks/appium-android-5.png)

Now your basic Android application is ready for test.

To be able to test an application, users need to either use a real Android device or an emulator.  
To setup emulator, navigate to **Tools -> Android -> AVD Manager**:

[![](../img/qa-frameworks/appium-android-6.png)](../img/qa-frameworks/appium-android-6.png)*
Click on **Create Virtual Device** button and add new Nexus 5X device:

[![](../img/qa-frameworks/appium-android-7.png)](../img/qa-frameworks/appium-android-7.png)

[![](../img/qa-frameworks/appium-android-8.png)](../img/qa-frameworks/appium-android-8.png)

**NOTE:** If selected system image is not available - click on "Download" link to download it.

[![](../img/qa-frameworks/appium-android-9.png)](../img/qa-frameworks/appium-android-9.png)

**NOTE:** Remember the name of device, e.g. "Nexus 5X API 24" - we will be using it for setting up Appium test.

Go back to your project in Android Studio and select **Run -> Run 'app'** menu item.    
In "Select Deployment Target" choose your virtual device and click OK.

[![](../img/qa-frameworks/appium-android-10.png)](../img/qa-frameworks/appium-android-10.png)

Now, your sample application shall appear in Android emulator:

[![](../img/qa-frameworks/appium-android-11.png)](../img/qa-frameworks/appium-android-11.png)

Now you are ready to start writing Appium tests.

## Launching Appium and inspect created application
Since, Appium works together with Selenium WebDriver, we need to know how WebDriver can find UI elements of our application.
One way is to launch Appium server, Appium Inspector and inspect created application.

To launch Appium server, launch Appium Desktop client:

[![](../img/qa-frameworks/appium-cli-1.png)](../img/qa-frameworks/appium-cli-1.png)

And click to "Start Server v1.6.5" (version, of course, may be different) button.  
Now Appium server is up and running.

[![](../img/qa-frameworks/appium-cli-2.png)](../img/qa-frameworks/appium-cli-2.png)

To inspect the application, click on **Start New Session** button and add desired capabilities.  
You can refer to [Appium documentation](https://github.com/appium/appium/blob/master/docs/en/writing-running-appium/caps.md) to learn more about Appium capabilities.  

For now, we can use just a minimum required set of capabilities to be able to inspect our application:

*	platformName: Android
*	avd: Nexus_5X_API_24 (the name of virtual device in AVD Manager)
*	deviceName: Android
*	appPackage: com.agiletestware.bumblebeetest (the name of your app package)
*	appActivity: .MainActivity (the name of your acrivity)

[![](../img/qa-frameworks/appium-cli-3.png)](../img/qa-frameworks/appium-cli-3.png)

After setting up capabilities, click on **Start Session** button and Appium will start your application on your virtual device and also inspector.  

Let's inspect element and find the id of the "Send" button:

[![](../img/qa-frameworks/appium-cli-4.png)](../img/qa-frameworks/appium-cli-4.png)

The id of the button is `com.agiletestware.bumblebeetest:id/fab`. Let's remember it as it will be used in our Appium test.

## Creating simple Appium test
### Configuring Java Maven project
#### Adding required dependencies
The following dependencies need to be added to your ==pom.xml== file:

```xml
<dependencies>
	<dependency>
		<groupId>junit</groupId>
		<artifactId>junit</artifactId>
		<version>4.12</version>
		<scope>test</scope>
	</dependency>
	<dependency>
		<groupId>com.agiletestware</groupId>
		<artifactId>bumblebee-annotations</artifactId>
		<version>0.1.4</version>
		<scope>test</scope>
	</dependency>
	<dependency>
		<groupId>org.seleniumhq.selenium</groupId>
		<artifactId>selenium-server</artifactId>
		<version>3.0.1</version>
	</dependency>
</dependencies>
<repositories>
	<repository>
		<id>nexus.agiletestware.com</id>
		<url>https://nexus.agiletestware.com/repository/maven-public</url>
	</repository>
</repositories>
```

#### Configure JUnit/TestNG plugins
##### JUnit:
``` xml
<build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-surefire-plugin</artifactId>
            <version>2.19</version>
            <configuration>
                <properties>
                    <property>
                        <name>listener</name>
                        <value>com.agiletestware.bumblebee.annotations.BumblebeeJUnitListener</value>
                    </property>
                </properties>
            </configuration>
        </plugin>
    </plugins>
</build>
```

##### TestNG:
``` xml
<build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-surefire-plugin</artifactId>
            <version>2.19</version>
            <configuration>
                <properties>
                    <property>
                        <name>listener</name>
                        <value>com.agiletestware.bumblebee.annotations.testng.BumblebeeTestNGReporter</value>
                    </property>
                </properties>
            </configuration>
        </plugin>
    </plugins>
</build>
```

### Bumblebee configuration file:
Create ==bumblebee_config.xml== file and put into your project root folder. Here is a description of configuration file:

``` xml
<?xml version="1.0"?>
<bumblebee>
    <!-- URL of the Bumblebee Server -->
    <bumblebee_url>http://server_name:port/bumblebee</bumblebee_url>
    <!-- URL of HP ALM Server -->
    <alm_url>http://server_name:port/qcbin</alm_url>
    <!-- Name of HP ALM User -->
    <alm_user>qcuser</alm_user>
    <!-- Encrypted password: please use http://server_name:port/bumblebee/password/encrypt to encrypt your plain text password  -->
    <alm_encrypted_pass>fd4OMOXLJjkMR6e64RJh3Q==</alm_encrypted_pass>
    <!-- HP ALM Domain -->
    <alm_domain>DEFAULT</alm_domain>
    <!-- HP ALM Project -->
    <alm_project>annotations_demo</alm_project>
    <!-- Asynchronous (offline) update -->
    <async_update>true</async_update>
</bumblebee>
```

### Creating a test class
Create a simple test class under src/test/java directory:

``` java
package com.agiletestware.bumblebee.dummytest;

import java.net.URL;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class AppiumTest {
	private static WebDriver driver;

	@BeforeClass
	public static void setUp() throws Exception {
		// define capabilities
		final DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("avd", "Nexus_5X_API_24");
		capabilities.setCapability("deviceName", "Android");
		capabilities.setCapability("appPackage", "com.agiletestware.bumblebeetest");
		capabilities.setCapability("appActivity", ".MainActivity");
		// use local Appium server
		driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
	}

	@Test
	public void testClickOnSendButton() {
		driver.findElement(By.id("com.agiletestware.bumblebeetest:id/fab")).click();
		Assert.assertEquals("something wrong", driver.findElement(By.id("com.agiletestware.bumblebeetest:id/snackbar_text")).getText());
	}

	@AfterClass
	public static void tearDown() {
		if (driver != null) {
			driver.quit();
		}
	}

}

```

This test will fail because expected text is not equal to the real text that is shown after clicking on the Send button.

## Sending test results to HP ALM
To send test results to HP ALM just add `@Bumblebee` annotation on class/method level:

**NOTE:** Please refer to [Bumblebee Selenium WebDriver](selenium-webdriver/#add-bumblebee-annotations-to-your-junittestng-test-classesmethods) documentation for a full description of `@Bumblebee` annotation package features.

``` java
package com.agiletestware.bumblebee.dummytest;

import java.net.URL;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.agiletestware.bumblebee.annotations.Bumblebee;

@Bumblebee(testplan = "Subject\\appium", testlab = "Root\\appium", testset = "appium test")
public class AppiumTest {
	private static WebDriver driver;

	@BeforeClass
	public static void setUp() throws Exception {
		// define capabilities
		final DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("avd", "Nexus_5X_API_24");
		capabilities.setCapability("deviceName", "Android");
		capabilities.setCapability("appPackage", "com.agiletestware.bumblebeetest");
		capabilities.setCapability("appActivity", ".MainActivity");
		// use local Appium server
		driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
	}

	@Test
	public void testClickOnSendButton() {
		driver.findElement(By.id("com.agiletestware.bumblebeetest:id/fab")).click();
		Assert.assertEquals("something wrong", driver.findElement(By.id("com.agiletestware.bumblebeetest:id/snackbar_text")).getText());
	}

	@AfterClass
	public static void tearDown() {
		if (driver != null) {
			driver.quit();
		}
	}

}
```

and run Maven test goal: `mvn test`

[![Maven output](../img/qa-frameworks/appium-maven-1.png)](../img/qa-frameworks/appium-maven-1.png)

And you should see the results in HP ALM TestPlan:

[![HP ALM TestPlan](../img/qa-frameworks/appium-results-1.png)](../img/qa-frameworks/appium-results-1.png)

HP ALM TestLab:

[![HP ALM TestLab](../img/qa-frameworks/appium-results-2.png)](../img/qa-frameworks/appium-results-2.png)

**NOTE:** Please note that TestPlan, TestLab folders and test set were created automatically by Bumblebee.  
