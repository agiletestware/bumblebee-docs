title: Serenity and HP ALM Integration Guide - Bumblebee Documentation
description: Bumblebee provides support for Serenty BDD framework allowing you to easiliy integrate it with HP ALM.

# Serenity  and HP ALM Integration Guide

Software teams often use [Serenity](http://www.thucydides.info/#/) to write automated acceptance tests and produce cool reports.
Sometimes they also want to see results of acceptance tests in HP ALM and this can be easily done with Bumblebee.

## Prerequisites

*	One of the popular CI server like [Jenkins](../ci-integration/jenkins.md), [Atlassian Bamboo](../ci-integration/bamboo.md) or [TeamCity](../ci-integration/teamcity.md) with corresponding Bumblebee plugin/add-on installed
*	Bumblebee Server must be installed and be accessible via HTTP from the machine where CI server/agent runs.

## Generate Serenity report in JSON format
Bumblebee works best with Serenity report in JSON format because it contains information about test steps and can also contain embedded screenshots. This report is generated by default when using [Serenity with JUnit](http://thucydides.info/docs/serenity-staging/#_serenity_with_junit)


## Setting up CI job to send results to HP ALM
Check CI corresponding pages for your CI system for initial Bumblebee configuration:

*	[Jenkins](../ci-integration/jenkins.md)
*	[Atlassian Bamboo](../ci-integration/bamboo.md)
*	[TeamCity](../ci-integration/teamcity.md)

In all following examples Jenkins CI and Maven build tool will be used, but it's more or less the same for other CI systems.

### Building Maven project in Jenkins
Assuming you have a Maven project with Serenity test which are run with JUnit framework, all we need to build it in Jenkins is add "Invoke top-level Maven targets" build step with `clean test` goals:

[![Jenkins configuration](../img/qa-frameworks/cucumber-1.png "Jenkins configuration")](../img/qa-frameworks/cucumber-1.png "Jenkins configuration")

### Adding new Bumblebee HP ALM Uploader post build action
To send generated serenity reports to HP ALM Reports, a "Bumblebee HP ALM Uploader" post build action should be added:

[![Jenkins configuration](../img/qa-frameworks/serenity-1.png "Jenkins configuration")](../img/qa-frameworks/serenity-1.png "Jenkins configuration")

!!! note
	Note, that "Format" field must have ==serenity== value and "Result File Pattern" must have path to a JSON report defined in Serenity test class.

### Execution and results
During Jenkins build Bumblebee prints information about progress into Jenkins console output:

[![Jenkins console output](../img/qa-frameworks/serenity-2.png "Jenkins console output")](../img/qa-frameworks/serenity-2.png "Jenkins console output")

#### Results in HP ALM TestPlan

[![HP ALM Results](../img/qa-frameworks/serenity-3.png "HP ALM Results")](../img/qa-frameworks/serenity-3.png "HP ALM Results")

!!! tip "Automatic folder structure creation"
	Folder structure is being automatically created by Bumblebee during exporting results, so there is no need to create it upfront

#### Results in HP ALM TestLab

[![HP ALM Results](../img/qa-frameworks/serenity-4.png "HP ALM Results")](../img/qa-frameworks/serenity-4.png "HP ALM Results")

[![HP ALM Results](../img/qa-frameworks/serenity-5.png "HP ALM Results")](../img/qa-frameworks/serenity-5.png "HP ALM Results")
