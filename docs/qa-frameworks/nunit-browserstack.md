title: BrowserStack and HP ALM Integration Guide - Bumblebee Documentation
description: Bumblebee provides a simple way to integrate HP ALM with BrowserStack. See how we do it in this guide.

# Integrating with [BrowserStack](https://www.browserstack.com)

Bumblebee provides a simple way to integrate HP ALM with [BrowserStack](https://www.browserstack.com) and allows users to:

*	Fetch test metadata from BrowserStack and publish it in HP ALM
*	Update actual status of a test in BrowserStack

## Visual Studio Project Setup
Please see [Visual Studio Project Setup](nunit-selenium-webdriver/#visual-studio-project-setup) section for details.

## BrowserStack Connection Details  
Add the following element to ==Bumblebee== section in App.config file:


```xml
<remoteTestTool>
	<browserStack
	 user="user"
	 apiKey="apiKey"/>
</remoteTestTool>
```

where ==user== is the BrowserStack username and ==apiKey== is BrowserStack user's API key.  
By default, Bumblebee will use https://www.browserstack.com/automate/ as a base URL for BrowserStack REST API calls. If needed, it can be changed by adding ==url== attribute into ==browserstack== element:

```xml
<remoteTestTool>
	<browserStack
	 url="https://someotherurl/automate/"
	 user="user"
	 apiKey="apiKey"/>
</remoteTestTool>
```

Example:

```xml
<configSections>
    <section name="Bumblebee"
             type="AgileTestware.Bumblebee.Core.Configurations.BumblebeeSection, AgileTestware.Bumblebee.Core"
             allowDefinition="Everywhere"
             allowLocation="true"/>  
</configSections>

<Bumblebee bumblebee_url="http://bumblebee_server:port/bumblebee"
            alm_url="http://alm_server:port/qcbin"
            alm_user="qcuser"
            alm_encrypted_pass="fd4OMOXLJjkMR6e64RJh3Q=="
            alm_domain="DEFAULT"
            alm_project="demo">
   <remoteTestTool>
     <browserStack user="user"
                   apiKey="key"/>
 </remoteTestTool>
</Bumblebee>
```

## Implementing AgileTestware.WebDriver.IWebDriverTest interface
In order to provide a IWebDriver instance to Bumblebee, your test should implement ==AgileTestware.WebDriver.IWebDriverTest== interface, which contains only one ==GetWebDriver()== method. This method must return a valid instance of IWebDriver which is being used for your tests, otherwise integration with BrowserStack will not work.

## Examples
### Export BrowserStack details to HP ALM
Here is a simple example on how to export BrowserStack execution data to HP ALM.  
In this example we will just run a couple of simple Selenium test in BrowserStack.

```csharp
using AgileTestware.Bumblebee.NUnit.Attributes;
using AgileTestware.WebDriver;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.ObjectModel;

namespace Agiletestware.Demo.NUnit
{
    [BumblebeeTestFixture(TestPlan = "Subject\\NUnit\\BrowserStack", TestLab = "Root\\NUnit", TestSet = "BrowserStack")]
    [TestFixture]
    class BrowserStackIntegration:IWebDriverTest
    {
        private IWebDriver webDriver;

        [SetUp]
        public void SetUp()
        {            
            DesiredCapabilities capability = new DesiredCapabilities();
            capability.SetCapability("browser", "Chrome");
            capability.SetCapability("browser_version", "65.0");
            capability.SetCapability("os", "Windows");
            capability.SetCapability("os_version", "7");
            capability.SetCapability("resolution", "1024x768");
            capability.SetCapability("browserstack.user", "user");
            capability.SetCapability("browserstack.key", "key");
            webDriver = new RemoteWebDriver(new Uri("http://hub-cloud.browserstack.com/wd/hub/"), capability);            
        }

        public IWebDriver GetWebDriver()
        {
            return webDriver;
        }

        [BumblebeeTest(TestName = "Pizza test", Description = "Test that the first Google result about pizza realy contains 'pizza' word")]
        [Test]
        public void TestFirstGoogleResultIsReallyAboutPizza()
        {
            ReadOnlyCollection<IWebElement> results = GetGoogleResults("pizza");
            results[0].Click();
            if (webDriver.WindowHandles.Count > 1)
            {
                webDriver.SwitchTo().Window(webDriver.WindowHandles[1]);
            }
            Assert.True(webDriver.PageSource.Contains("pizza"), "Why the first google result for 'pizza' does not contain a single word about pizza?");
        }

        [BumblebeeTest(TestName = "Verify no bad pizzas", Description = "Verify that we live in the ideal world where all pizzas are good")]
        [Test]
        public void TestBadPizzasDoNotExist()
        {
            ReadOnlyCollection<IWebElement> results = GetGoogleResults("bad pizza");
            if (results.Count > 1)
            {
                throw new Exception("Liar, there are no bad pizzas!");
            }
        }

        protected ReadOnlyCollection<IWebElement> GetGoogleResults(String query)
        {
            webDriver.Manage().Window.Maximize();
            webDriver.Navigate().GoToUrl("http://google.com");
            IWebElement element = webDriver.FindElement(By.Name("q"));
            element.SendKeys(query);
            element.Submit();
            IWebElement myDynamicElement = (new WebDriverWait(webDriver, TimeSpan.FromSeconds(10))
           .Until((d) =>
           {
               IWebElement el = d.FindElement(By.Id("resultStats"));
               if (el.Displayed && el.Enabled)
               {
                   return el;
               }
               return null;
           }));
            return webDriver.FindElements(By.XPath("//*[@id='rso']//h3/a"));
        }

        [TearDown]
        public void TearDown()
        {
            if (webDriver != null)
            {
                webDriver.Close();
                webDriver.Quit();
            }
        }
    }
}
```

Results in HP ALM TestPlan:

[![Result in HP ALM](../img/qa-frameworks/NUnit-browserstack-1.png "Result in HP ALM")](../img/qa-frameworks/NUnit-browserstack-1.png "Result in HP ALM")

After execution a new run is created for each test in HP ALM TestLab:

[![Result in HP ALM](../img/qa-frameworks/NUnit-browserstack-2.png "Result in HP ALM")](../img/qa-frameworks/NUnit-browserstack-2.png "Result in HP ALM")

Each step contains all related BrowserStack URLs (test link, video and logs), so you can easily check test run in BrowserStack if needed:

[![Result in HP ALM](../img/qa-frameworks/NUnit-browserstack-3.png "Result in HP ALM")](../img/qa-frameworks/NUnit-browserstack-3.png "Result in HP ALM")

By default, Bumblebee also updates the status of a test in BrowserStack, so if test fails, failure reason can be seen in BrowserStack as well as in HP ALM:

[![Result in BrowserStack](../img/qa-frameworks/NUnit-browserstack-4.png "Result in HP ALM")](../img/qa-frameworks/NUnit-browserstack-4.png "Result in BrowserStack")

### Automatic screenshot capturing for failed tests
Selenium WebDriver tests failures are hard to debug if you only look at the stacktrace. Bumblebee can be configured to automatically capture screenshots for failed tests and send them to HP ALM. This is super useful for quick debugging.

**Bumblebee Configuration**

To enable screenshots, add the ==screenshotOnFailure== attribute to the Bumblebee configuration section in App.config file of your solution:

```xml
<Bumblebee 	 		
	bumblebee_url="http://bumblebee_server:port/bumblebee"	 		
	alm_url="http://alm_server:port/qcbin"
    alm_user="user"
    alm_encrypted_pass="fd4OMOXLJjkMR6e64RJh3Q=="
    alm_domain="DEFAULT"
    alm_project="project"
    screenshotOnFailure="true">

    <remoteTestTool>
		<browserStack
			user="user"
	 		apiKey="apiKey"/>
	</remoteTestTool>
</Bumblebee>
```

With this feature turned on, Bumblebee just captures screenshot at the moment of test failure and adds it to HP ALM run:

[![Result in HP ALM](../img/qa-frameworks/NUnit-browserstack-5.png "Result in HP ALM")](../img/qa-frameworks/NUnit-browserstack-5.png "Result in HP ALM")

### Disabling BrowserStack status update
If updating of BrowserStack test status is not desired by some reasons, it can be easily switched off by setting ==updateStatus== attribute of ==browserStack== element to **false**:  

```xml
<Bumblebee 	 		
	bumblebee_url="http://bumblebee_server:port/bumblebee"	 		
	alm_url="http://alm_server:port/qcbin"
    alm_user="user"
    alm_encrypted_pass="fd4OMOXLJjkMR6e64RJh3Q=="
    alm_domain="DEFAULT"
    alm_project="project"
    screenshotOnFailure="true">

    <remoteTestTool>
		<browserStack
			user="user"
	 		apiKey="apiKey"
	 		updateStatus="false"/>
	</remoteTestTool>
</Bumblebee>
```
In such a case you will see ==Skipping BrowserStack status update.== message in test output and test status in BrowserStatus will not be updated:

[![Result in HP ALM](../img/qa-frameworks/NUnit-browserstack-5.png "Result in HP ALM")](../img/qa-frameworks/NUnit-browserstack-5.png "Result in HP ALM")

### Disabling exporting BrowserStack data to HP ALM
In case if BrowserStack data is not needed in HP ALM, you can either:

*	Remove the whole ==remoteTestTool== element from App.config
*	Or set ==isEnabled== attribute of ==browserStack== element to **false**

E.g.:

```xml
<Bumblebee 	 		
	bumblebee_url="http://bumblebee_server:port/bumblebee"	 		
	alm_url="http://alm_server:port/qcbin"
    alm_user="user"
    alm_encrypted_pass="fd4OMOXLJjkMR6e64RJh3Q=="
    alm_domain="DEFAULT"
    alm_project="project"
    screenshotOnFailure="true">

    <remoteTestTool>
		<browserStack
			user="user"
	 		apiKey="apiKey"
	 		isEnabled="false"/>
	</remoteTestTool>
</Bumblebee>
```

In such a case, you will get ==Skipping BrowserStack update. BrowserStack integration is disabled.== record in test output and tests in HP ALM will not get any BrowserStack related data.
