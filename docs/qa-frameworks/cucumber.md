title: Cucumber and HP ALM Integration Guide - Bumblebee Documentation
description: Bumblebee provides an easy way to integrate Cucumber with HP ALM

# Cucumber and HP ALM Integration Guide

Software teams often use BDD approach for developing their applications. And one of the most popular tool for that is [Cucumber](https://cucumber.io/) . On the other hand, some companies use HP ALM for tracking their projects and want to see test results there.  

An integration between Cucumber and HP ALM can be a tricky and time consuming, so Bumblebee helps to solve this problem with a couple of clicks.

## Prerequisites

*	One of the popular CI server like [Jenkins](../ci-integration/jenkins.md), [Atlassian Bamboo](../ci-integration/bamboo.md) or [TeamCity](../ci-integration/teamcity.md) with corresponding Bumblebee plugin/add-on installed
*	Bumblebee Server must be installed and be accessible via HTTP from the machine where CI server/agent runs.

## Generate Cucumber report in JSON format
Bumblebee works best with Cucumber report in JSON format because it contains information about test steps and can also contain embedded screenshots.  
To generate such report when running Cucumber tests together with JUnit, the one just need to add the following annotation to the main cucumber class:

```java
@CucumberOptions(plugin = { "json:target/test-report.json" })
```
Where `target/test-report.json` is the path to the report which will be produced.

e.g.:

```java
@RunWith(Cucumber.class)
@CucumberOptions(plugin = { "json:target/test-report.json" })
public class Tests {

}
```

## Setting up CI job to send results to HP ALM
Check CI corresponding pages for your CI system for initial Bumblebee configuration:

*	[Jenkins](../ci-integration/jenkins.md)
*	[Atlassian Bamboo](../ci-integration/bamboo.md)
*	[TeamCity](../ci-integration/teamcity.md)

In all following examples Jenkins CI and Maven build tool will be used, but it's more or less the same for other CI systems.

### Building Maven project in Jenkins
Assuming you have a Maven project with Cucumber test which are run with JUnit framework, all we need to build it in Jenkins is add "Invoke top-level Maven targets" build step with `clean test` goals:

[![Jenkins configuration](../img/qa-frameworks/cucumber-1.png "Jenkins configuration")](../img/qa-frameworks/cucumber-1.png "Jenkins configuration")

### Adding new Bumblebee HP ALM Uploader post build action
To send cucumber reports to HP ALM Reports, a "Bumblebee HP ALM Uploader" post build action should be added:

[![Jenkins configuration](../img/qa-frameworks/cucumber-2.png "Jenkins configuration")](../img/qa-frameworks/cucumber-2.png "Jenkins configuration")

!!! note
	Note, that "Format" field must have ==cucumber== value and "Result File Pattern" must have path to a JSON report defined in Cucumber test class.

### Execution and results
During Jenkins build Bumblebee prints information about progress into Jenkins console output:

[![Jenkins console output](../img/qa-frameworks/cucumber-3.png "Jenkins configuration")](../img/qa-frameworks/cucumber-3.png "Jenkins console output")

#### Results in HP ALM TestPlan

[![HP ALM Results](../img/qa-frameworks/cucumber-test-plan-1.png "Jenkins configuration")](../img/qa-frameworks/cucumber-test-plan-1.png "HP ALM Results")

!!! tip "Automatic folder structure creation"
	Folder structure is being automatically created by Bumblebee during exporting results, so there is no need to create it upfront

#### Results in HP ALM TestLab

[![HP ALM Results](../img/qa-frameworks/cucumber-test-lab-1.png "Jenkins configuration")](../img/qa-frameworks/cucumber-test-lab-1.png "HP ALM Results")

[![HP ALM Results](../img/qa-frameworks/cucumber-test-lab-2.png "Jenkins configuration")](../img/qa-frameworks/cucumber-test-lab-2.png "HP ALM Results")

!!! note "Screenshot attachments"
	If cucumber report contains embedded attachments, they will be attached to a test step in HP ALM

[![HP ALM Results](../img/qa-frameworks/cucumber-test-lab-3.png "Jenkins configuration")](../img/qa-frameworks/cucumber-test-lab-3.png "HP ALM Results")
