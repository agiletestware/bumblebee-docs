title: Selenium WebDriver & HP ALM Integration Guide - Bumblebee Documentation
description: Bumblebee's JUnit and TestNG solution allows you to easily integrate Selenium WebDriver tests with HP ALM. See how we do it in this guide.

# Selenium WebDriver + HP ALM Integration Guide

!!! success ""
    [Selenium WebDriver](http://www.seleniumhq.org/projects/webdriver/) is one of the most widely used open source tool for automating browser interaction and is used by countless companies.

    Bumblebee's JUnit and TestNG solution allows you to easily integrate Selenium WebDriver tests with [HP ALM](https://saas.hpe.com/en-us/software/application-lifecycle-management), map them to requirements, and automatically upload tests, and test results to HP ALM Testplan, TestLab, and Requirements Module.


!!! note "Install Bumblebee Server before configuring maven projects"
    Bumblebee's JUnit and TestNG solution communicates with HP ALM via the Bumblebee server. You must install Bumblebee server before configuring your maven project. [Bumblebee server setup instructions](../setup/server-installation.md)

## Project Setup

### Maven
Modify your ==pom.xml== file and add agiletestware repository and bumblebee-annotations dependency:

``` xml
<dependencies>
    <dependency>
        <groupId>com.agiletestware</groupId>
        <artifactId>bumblebee-annotations</artifactId>
        <version>0.1.4</version>
        <scope>test</scope>
    </dependency>
</dependencies>
<repositories>
	<repository>
		<id>nexus.agiletestware.com</id>
		<url>https://nexus.agiletestware.com/repository/maven-public</url>
	</repository>
</repositories>
```
Configure Maven surefire plugin for JUnit or TestNG frameworks.


JUnit Maven surefire plugin config:

``` xml
<build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-surefire-plugin</artifactId>
            <version>2.19</version>
            <configuration>
                <properties>
                    <property>
                        <name>listener</name>
                        <value>com.agiletestware.bumblebee.annotations.BumblebeeJUnitListener</value>
                    </property>
                </properties>
            </configuration>
        </plugin>
    </plugins>
</build>
```

TestNG Maven surefire plugin config:

``` xml
<build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-surefire-plugin</artifactId>
            <version>2.19</version>
            <configuration>
                <properties>
                    <property>
                        <name>listener</name>
                        <value>com.agiletestware.bumblebee.annotations.testng.BumblebeeTestNGReporter,com.agiletestware.bumblebee.annotations.testng.BumblebeeTestNGListener</value>
                    </property>
                </properties>
            </configuration>
        </plugin>
    </plugins>
</build>
```

!!! note "Configure TestNG listeners without Maven"
	It is possible to use Bumblebee TestNG listeners outside of Maven build which brings possibility to run tests and export their results to ALM for single test and right from your favorite IDE.

To do so, you can either add listeners to your test class:

```java
	
import org.testng.annotations.Listeners;
import com.agiletestware.bumblebee.annotations.testng.BumblebeeTestNGListener;
import com.agiletestware.bumblebee.annotations.testng.BumblebeeTestNGReporter;
	
@Listeners(value = { BumblebeeTestNGReporter.class, BumblebeeTestNGListener.class })
public class BaseClass {
}				
```

or define them in TestNG suite XML file:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<suite name="Bumblebee test suite" parallel="false">
	<listeners>
		<listener class-name="com.agiletestware.bumblebee.annotations.testng.BumblebeeTestNGListener" />
		<listener class-name="com.agiletestware.bumblebee.annotations.testng.BumblebeeTestNGReporter" />
	</listeners>
	<test name="Test">
		<classes>
			<class
				name="com.agiletestware.bumblebee.dummytest.MethodsAsStepsTest" />
		</classes>
	</test>
</suite>
```

!!! note "Please note that in such a case listeners are added globally and apply to the whole TestNG suite"

### Configuration file

Bumblebee configuration file contains information about your HP ALM setup. The configuration values are read by Bumblebee's JUnit and TestNG solution and used to update test results in HP ALM.

Create ==bumblebee_config.xml== file and put into the project root folder. Example:

``` xml
<?xml version="1.0"?>
<bumblebee>
    <!-- URL of the Bumblebee Server -->
    <bumblebee_url>http://server_name:port/bumblebee</bumblebee_url>
    <!-- URL of HP ALM Server -->
    <alm_url>http://server_name:port/qcbin</alm_url>
    <!-- Name of HP ALM User -->
    <alm_user>qcuser</alm_user>
    <!-- Encrypted password: please use http://server_name:port/bumblebee/password/encrypt to encrypt your plain text password  -->
    <alm_encrypted_pass>fd4OMOXLJjkMR6e64RJh3Q==</alm_encrypted_pass>
    <!-- HP ALM Domain -->
    <alm_domain>DEFAULT</alm_domain>
    <!-- HP ALM Project -->
    <alm_project>annotations_demo</alm_project>
    <!-- Asynchronous (offline) update -->
    <async_update>false</async_update>
</bumblebee>
```

### Overriding configuration options with Java Properties (since 0.1.3)
Due to security or other reasons, you might want to override or replace XML configuration with Java Properties.

The following Java configuration properties are available:

| Name								 | Description 																						 |
|------------------------------------|---------------------------------------------------------------------------------------------------|
| bumblebee.config					 | Path to a configuration file. If not present, then ${project.basedir}/bumblebee_config.xml is used|
| bumblebee.config.bumblebee_url	 | URL of the Bumblebee Server          															 |
| bumblebee.config.alm_url			 | URL of ALM Server 																				 |
| bumblebee.config.alm_user			 | Name of ALM User 																				 |
| bumblebee.config.alm_password		 | Encrypted user password 																			 |
| bumblebee.config.alm_domain		 | Name of ALM Domain 																				 |
| bumblebee.config.alm_project		 | Name of ALM Project 																				 |
| bumblebee.config.async_update		 | Asynchronous (offline) update																	 |
| bumblebee.config.take_screenshot   | Boolean flag indicating whether a screenshot of a failure should be automatically done (for WebDriver tests only) |
| bumblebee.config.enabled			 | Boolean flag indicating whether Bumblebee functionality is enabled or not						 |
| bumblebee.config.update_testplan	 | Boolean flag indicating whether Bumblebee should update existing tests in ALM TestPlan module	 |

To provide values to Maven via command line, please use -D<property_name>, e.g.


```batch
mvn clean test -Dbumblebee.config.bumblebee_url=http://localhost:8080/bumblebee
```

!!! note "Note"
	Values provided with Java Properties override the ones provided in XML configuration.



## Bumblebee Annotations
Bumblebee provides specialized annotations package that allows users to map JAVA tests to ALM tests and export their test results. ==@Bumblebee== Java annotation can be added on JUnit/TestNG class or method level.

### Annotation parameters

| Name | Description | Applicable | Required |
|-------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------|----------|
| testplan | Path to test in a Test Plan in HP ALM. e.g. Subject\Test1 | Class, method | Yes |
| testlab | Path to test set in a test lab in HP ALM. e.g. Root\Test1 | Class, method | Yes |
| testset | Name of test set in HP ALM | Class, method | Yes |
| testname | If specified, then the value of testname parameter will be set as the,name of test in HP ALM. If not set, test name will be set to fully,qualified method name, e.g. com.annotations.Demo.method1 | Method | No |
| almid | Defines id of a test in a HP ALM test plan which needs to be updated. If specified then testplan and testname are ignored | Method | No |
| description | Description for test in HP ALM | Method | No |
| overwriteAlmSteps | If set to BooleanValue.TRUE, all existing test steps will be deleted | Class, method | No |
| parameters | An array of custom parameters which are passed to the bumblebee server and then mapped to HP ALM custom fields. Please refer to [documentation](../setup/alm-mappings.md "documentation") documentation on how to setup mappings on Bumblebee server | Class, method | No |
| hasSteps | ==Works with TestNG only== If set to BooleanValue.TRUE, test class is exported as a single test in HP ALM, and all its test methods are exported as test steps within the test | No |

**Bumblebee annotation on method level overrides values set by Bumblebee annotation on class level.**

### Defining dynamic values for annotation parameters (since 0.1.0)
The following parameters can contain dynamic values:  

*	testplan
*	testlab
*	testset
*	testname
*	description

To define a dynamic value, just add a placeholder in format: `${property_name}`, where `property_name` is the name of property containing value for the parameter.    
Bumblebee resolves dynamic values in run time using Java System Properties or XML configuration file.  

#### Providing values with bumblebee_config.xml file
To set a value for dynamic parameter in bumblebee_config.xml file, just add new `property` element. E.g. to provide a value for ${"example"} placeholer

``` java
@Bumblebee(testset="Test set: ${example}")
```

```xml
<property name="example">some value</property>
```

#### Providing values with Java System Properties
To provide a value with Java System Property, just pass then into your Maven build, e.g.

``` batch
mvn clean test -Dexample=Something
```

If both XML configuration and Java System Property are provided, value in Java System Property will be used.

To run JUnit/TestNG tests and send results to HP ALM, simply run Maven test phase on your project:
Example: ==mvn test==


!!! note
    Unless stated, all the examples below are for JUnit. To run the same example on TestNG, simply replace ==org.junit.Test== with ==org.testng.annotations.Test==
    Imports are omitted for brevity.


### Class Level Annotations
In this example, we have added the annotations to the class. This means that all the tests within this class will inherit the same testplan, testlab, testset, etc values.

``` java
package com.agiletestware.bumblebee.dummytest;

@Bumblebee(testplan = "Subject\\webdriver", testlab = "Root\\webdriver", testset = "class annotations")
public class WebDriverTest {

	private WebDriver driver;

	@BeforeClass
	public static void beforeClass() {
		System.setProperty("webdriver.chrome.driver", new File("chromedriver.exe").getAbsolutePath());
	}

	@Before
	public void setUp() {
		this.driver = new ChromeDriver();
	}

	@Test
	public void testOne() {
		driver.get("http:\\agiletestware.com");
		Assert.assertEquals("Agiletestware - Software for QA and Development Tools", driver.getTitle());
	}

	@Test
	public void testTwo() {
		driver.get("http:\\agiletestware.com");
		Assert.assertEquals("Something wrong", driver.getTitle());
	}

	@Ignore@Test
	public void ignoredTest() {
		driver.get("http:\\agiletestware.com");
	}

	@Test
	public void testThree() {
		throw new RuntimeException("You shall not pass!!!");
	}

	@After
	public void tearDown() {
		driver.quit();
	}
}
```

When the above example is executed, the results will be automaticaly posted to HP ALM.

[![Results in HP ALM](../img/qa-frameworks/Selenium-class-annotations.png)](../img/qa-frameworks/Selenium-class-annotations.png)

### Method Level Annotations
In some situations, you may not want to export all the tests or you may want to group tests in different testplan, testlab, testsets. By applying Bumblebee annotations to methods, the user can have more granular control.

``` java
public class WebDriverTest {

	private WebDriver driver;

	@BeforeClass
	public static void beforeClass() {
		System.setProperty("webdriver.chrome.driver", new File("chromedriver.exe").getAbsolutePath());
	}

	@Before
	public void setUp() {
		this.driver = new ChromeDriver();
	}

	@Bumblebee(testname = "testOne", testplan = "Subject\\webdriver", testlab = "Root\\webdriver", testset = "method annotations")@Test
	public void testOne() {
		driver.get("http:\\agiletestware.com");
		Assert.assertEquals("Agiletestware - Software for QA and Development Tools", driver.getTitle());
	}

	@Test
	public void testTwo() {
		driver.get("http:\\agiletestware.com");
		Assert.assertEquals("Something wrong", driver.getTitle());
	}

	@Ignore@Test
	public void ignoredTest() {
		driver.get("http:\\agiletestware.com");
	}

	@Test
	public void testThree() {
		throw new RuntimeException("You shall not pass!!!");
	}

	@After
	public void tearDown() {
		driver.quit();
	}
}
```

When the above example is executed, only ==testOne()== will be exported to HP ALM.

[![Results in HP ALM](../img/qa-frameworks/Selenium-method-annotations.png "Results in HP ALM")](../img/qa-frameworks/Selenium-method-annotations.png)

### Overriding annotation values
In some situations, you may class-level annotations but may not want to apply them to all the methods. In such a situation, you can provide both class-level and method-level annotations to override the class-level values.

``` java
//class-level annotations values
@Bumblebee(testplan = "Subject\\webdriver", testlab = "Root\\webdriver", testset = "overridden values")
public class WebDriverTest {

	private WebDriver driver;

	@BeforeClass
	public static void beforeClass() {
		System.setProperty("webdriver.chrome.driver", new File("chromedriver.exe").getAbsolutePath());
	}

	@Before
	public void setUp() {
		this.driver = new ChromeDriver();
	}

	@Test
	public void testOne() {
		driver.get("http:\\agiletestware.com");
		Assert.assertEquals("Agiletestware - Software for QA and Development Tools", driver.getTitle());
	}

    //method-level annotations values
	@Bumblebee(testname = "test two", testplan = "Subject\\webdriver", testlab = "Root\\webdriver", testset = "overridden values")@Test
	public void testTwo() {
		driver.get("http:\\agiletestware.com");
		Assert.assertEquals("Something wrong", driver.getTitle());
	}

	@Ignore@Test
	public void ignoredTest() {
		driver.get("http:\\agiletestware.com");
	}

	@Test
	public void testThree() {
		throw new RuntimeException("You shall not pass!!!");
	}

	@After
	public void tearDown() {
		driver.quit();
	}

}
```

When the above example is executed, testOne() and testThree() will use class-level annotation values. testTwo() will use method-level annotation values.

[![Results in HP ALM](../img/qa-frameworks/Selenium-overriden-annotations.png "Results in HP ALM")](../img/qa-frameworks/Selenium-overriden-annotations.png)  

### Map test method to existing test in HP ALM
In many companies, it is very common for business analysts or manual test engineers write ==manual tests== in HP ALM. These manual tests are then automated at a later stage.  Bumblebee annotation can be used to help implement this workflow. As tests are automated in JAVA, they can be mapped to the manual tests in HP ALM.

For Example: Here is the existing test in HP ALM Test Plan with ==id = 112==

[![Map to existing test](../img/qa-frameworks/Selenium-map-existing-1.png "Map to existing test")](../img/qa-frameworks/Selenium-map-existing-1.png)  

It contains three design steps:

[![Map to existing test](../img/qa-frameworks/Selenium-map-existing-2.png "Map to existing test")](../img/qa-frameworks/Selenium-map-existing-2.png)  

To map this manual ALM test to a test method in JAVA, use the Bumblebee annotation's ==almid== parameter along with the testlab and testset values.

``` java
@Bumblebee(testplan = "Subject\\webdriver", testlab = "Root\\webdriver", testset = "overridden values")
public class WebDriverTest {

	private WebDriver driver;

	@BeforeClass
	public static void beforeClass() {
		System.setProperty("webdriver.chrome.driver", new File("chromedriver.exe").getAbsolutePath());
	}

	@Before
	public void setUp() {
		this.driver = new ChromeDriver();
	}

	@Test
	public void testOne() {
		driver.get("http:\\agiletestware.com");
		Assert.assertEquals("Agiletestware - Software for QA and Development Tools", driver.getTitle());
	}

	@Bumblebee(almid = 112, testlab = "Root\\webdriver", testset = "overridden values", description = "description")@Test
	public void testTwo() {
		driver.get("http:\\agiletestware.com");
		Assert.assertEquals("Something wrong", driver.getTitle());
	}

	@Ignore@Test
	public void ignoredTest() {
		driver.get("http:\\agiletestware.com");
	}

	@Test
	public void testThree() {
		throw new RuntimeException("You shall not pass!!!");
	}

	@After
	public void tearDown() {
		driver.quit();
	}

}
```

In the example above, testTwo() has been mapped to the manual testcase in ALM and also updated the test description using the ==description== annotation parameter.

[![Result in HP ALM](../img/qa-frameworks/Selenium-map-existing-3.png "Results in HP ALM")](../img/qa-frameworks/Selenium-map-existing-3.png)  

There is also a new test step with name Test Results:

[![Result in HP ALM](../img/qa-frameworks/Selenium-map-existing-4.png "Results in HP ALM")](../img/qa-frameworks/Selenium-map-existing-4.png)  

And results are exported to the HP ALM Test Lab:

[![Result in HP ALM](../img/qa-frameworks/Selenium-map-existing-5.png "Results in HP ALM")](../img/qa-frameworks/Selenium-map-existing-5.png)  

## Custom Fields Configuration
Most HP ALM projects have some custom user-defined fields. If these fields are **required**, they must be specified by the user. Fortunately, Bumblebee annotation frameworks make it super easy to configure custom user fields. Bumblebee annotation framework allows users to pass custom parameters to Bumblebee Server which contains the mapping to the custom user fields in HP ALM.

!!! summary "Bumblebee server mapping format"
    This is the format for specifying ALM's custom user fields in the Bumblebee server configuration. In this example, ==TS_USER_01== is the actual name of a custom field in ALM, ==label== is the user friendly key that will be used within the JAVA annotations for mapping.  

```xml
<mapping alm_field="TS_USER_01" label="user field"/>
```

Please refer to [ALM custom fields](../setup/alm-mappings.md "documentation") for detailed steps on how to setup mappings on Bumblebee server.

Example:
Lets say we have a custom text field: ==TS\_USER\_01== field in HP ALM and we want to set this field to ==value from Java==. Since we have already mapped this on Bumblebee server, we can use the ==label== value ==user field== in our JAVA annotation to complete the mapping.  

``` java
@Bumblebee(testplan = "Subject\\webdriver", testlab = "Root\\webdriver", testset = "overridden values")
public class WebDriverTest {

	private WebDriver driver;

	@BeforeClass
	public static void beforeClass() {
		System.setProperty("webdriver.chrome.driver", new File("chromedriver.exe").getAbsolutePath());
	}

	@Before
	public void setUp() {
		this.driver = new ChromeDriver();
	}

	@Bumblebee(almid = 112, testlab = "Root\\webdriver", testset = "overridden values", description = "description", parameters = {@Parameter(name = "user field", value = "value from Java")
	})@Test
	public void testTwo() {
		driver.get("http:\\agiletestware.com");
		Assert.assertEquals("Something wrong", driver.getTitle());
	}

	@After
	public void tearDown() {
		driver.quit();
	}
}
```

Once this test is executed, the mappings values are passed from the tests to the Bumblebee server and configured at run time.

[![Result in HP ALM](../img/qa-frameworks/Selenium-map_test_field.png "Results in HP ALM")](../img/qa-frameworks/Selenium-map_test_field.png)  

!!! tip
    Bumblebee annotations are very flexible and powerful and can be used to set custom fields for HP ALM Test, Test Set, Run, Design Step, Run Step, Test Instance.

## Overriding Bumblebee custom fields configuration
It is also possible to override server side configuration with the bumblebee XML configuration. To do that, just add a new set of ==mapping== elements into configuration:

```xml
<bumblebee>
	<bumblebee_url>http://bbe:8888/bumblebee</bumblebee_url>
	<alm_url>http://alm:8080/qcbin</alm_url>
	<alm_user>user</alm_user>
	<alm_encrypted_pass>fd4OMOXLJjkMR6e64RJh3Q==</alm_encrypted_pass>
	<alm_domain>DEFAULT</alm_domain>
	<alm_project>demo</alm_project>
	
	<mapping field_type="test" field_label="user field" bumblebee_label="test user field" default_value="something"/>	
</bumblebee>
```

==mapping== element has the following attributes:

*	**field_type** - a type of entity in ALM for which mapping is provided. The following are supported:
	*	**test** - test in TestPlan
	*	**test-set** - test set in TestLab
	*	**test-instance** - test instance in TestLab
	*	**run-step** - step of a test in TestPlan
	*	**step** - step of a test in a run in TestLab
	*	**run** - run in TestLab
*	**field_label** - label of a field in ALM (not a system name as in mapping configuration on server), e.g. 'Status' or 'User field', etc...
*	**bumblebee_label** - this is a label which should be used as @Parameter.name to pass a different value
*	**default_value** - default value

E.g. if you wish to provide value for test's "User field", the following element should be used:

```xml

<mapping field_type="test" field_label="user field" bumblebee_label="test user field" default_value="something"/>

```

When running Maven build, the following notification is displayed in the console output:

```text
Bumblebee: Passing mappings configuration to the server: [Mapping [fieldLabel=user field, fieldType=TEST, defaultValue=something, bumblebeeLabel=test user field]]
Bumblebee: Mapping configuration on Bumblebee Server will be ignored	
```

And results in ALM:

[![Result in HP ALM](../img/qa-frameworks/JUnit-custom-mappings-1.png "Results in HP ALM")](../img/qa-frameworks/JUnit-custom-mappings-1.png) 


## Exporting all test methods into one HP ALM test (TestNG only)
Sometimes tests classes are designed in such a way that each of them represents a single test case and test methods are just test steps. If you have such a test design, Bumblebee can help map this to HP ALM.

To export all methods of a single test class to HP ALM, mark the class with `@Bumblebee` annotation with `hasSteps` attribute set to `BooleanValue.TRUE`.  

Example:

``` java
@Bumblebee(testplan = "Subject\\webdriver", testlab = "Root\\webdriver", testset = "overridden values", hasSteps = BooleanValue.TRUE)
public class WebDriverTest {

	private WebDriver driver;

	@BeforeClass
	public static void beforeClass() {
		System.setProperty("webdriver.chrome.driver", new File("chromedriver.exe").getAbsolutePath());
	}

	@BeforeTest
	public void setUp() {
		this.driver = new ChromeDriver();
	}

	@Test(priority = 1)
	public void testOne() {
		driver.get("http:\\agiletestware.com");
		Assert.assertEquals("Agiletestware - Software for QA and Development Tools", driver.getTitle());
	}

	@Test(priority = 2)
	public void testTwo() {
		driver.get("http:\\agiletestware.com");
		Assert.assertEquals("Something wrong", driver.getTitle());
	}

	@Test(enabled = false)
	public void ignoredTest() {
		driver.get("http:\\agiletestware.com");
	}

	@Test(priority = 3)
	public void testThree() {
		throw new RuntimeException("You shall not pass!!!");
	}

	@AfterTest
	public void tearDown() {
		driver.quit();
	}
}
```

In such a case, the new test with three steps will be created in HP ALM Test Plan:  

[![Result in HP ALM](../img/qa-frameworks/Selenium-hasSteps-1.png "Result in HP ALM")](../img/qa-frameworks/Selenium-hasSteps-1.png)  

and results will also appear in Test Lab:

[![Result in HP ALM](../img/qa-frameworks/Selenium-hasSteps-2.png "Result in HP ALM")](../img/qa-frameworks/Selenium-hasSteps-2.png)  

!!! note
    Bumblebee relies on test method execution order provided by TestNG, so you have to provide a fixed test methods execution order somehow (e.g. via priorities). Otherwise your test steps will have incorrect execution order and consequently wrong order in HP ALM.


By default, test step names are the same as names of corresponding test methods in your Java code.  To change test step names and some other export settings, you can use the `@BumblebeeTestStep`. This annotation can be applied to test methods only and has the following properties:  

| Name | Description | Required |
|--------------------|-----------------|-----|
| name | Name of a test step in HP ALM | Yes |
| description | Description of a test step in HP ALM | No |
| expected | Value for `Expected` field of a test step in HP ALM | No|

Example:

``` java
@Bumblebee(testplan = "Subject\\webdriver", testlab = "Root\\webdriver", testset = "overridden values", hasSteps = BooleanValue.TRUE)
public class WebDriverTest {

	private WebDriver driver;

	@BeforeClass
	public static void beforeClass() {
		System.setProperty("webdriver.chrome.driver", new File("chromedriver.exe").getAbsolutePath());
	}

	@BeforeTest
	public void setUp() {
		this.driver = new ChromeDriver();
	}

	@Test(priority = 1)
	public void testOne() {
		driver.get("http:\\agiletestware.com");
		Assert.assertEquals("Agiletestware - Software for QA and Development Tools", driver.getTitle());
	}

	@Test(priority = 2)
	public void testTwo() {
		driver.get("http:\\agiletestware.com");
		Assert.assertEquals("Something wrong", driver.getTitle());
	}

	@Test(enabled = false)
	public void ignoredTest() {
		driver.get("http:\\agiletestware.com");
	}

	@BumblebeeTestStep(name = "test three", description = "test three description", expected = "expect something")@Test(priority = 3)
	public void testThree() {
		throw new RuntimeException("You shall not pass!!!");
	}

	@AfterTest
	public void tearDown() {
		driver.quit();
	}
}
```

The example above produces the following test steps in HP ALM Test Plan:  

[![Result in HP ALM](../img/qa-frameworks/Selenium-hasSteps-3.png "Result in HP ALM")](../img/qa-frameworks/Selenium-hasSteps-3.png)  

## Capturing Screenshots
Selenium WebDriver tests failures are hard to debug if you only look at the stacktrace. Bumblebee can be configured to automatically capture screenshots for failed tests and send them to HP ALM. This is super useful for quick debugging.

**Bumblebee Configuration**

To enable screenshots, add the ==screenshotOnFailure== element to the Bumblebee configuration.

Example:

```xml
<?xml version="1.0"?>
<bumblebee>
    <!-- URL of the Bumblebee Server -->
    <bumblebee_url>http://server_name:port/bumblebee</bumblebee_url>
    <!-- URL of HP ALM Server -->
    <alm_url>http://server_name:port/qcbin</alm_url>
    <!-- Name of HP ALM User -->
    <alm_user>qcuser</alm_user>
    <!-- Encrypted password: please use http://server_name:port/bumblebee/password/encrypt to encrypt your plain text password  -->
    <alm_encrypted_pass>fd4OMOXLJjkMR6e64RJh3Q==</alm_encrypted_pass>
    <!-- HP ALM Domain -->
    <alm_domain>DEFAULT</alm_domain>
    <!-- HP ALM Project -->
    <alm_project>annotations_demo</alm_project>
    <!-- Asynchronous (offline) update -->
    <async_update>true</async_update>    
    <!-- Capture screenshot on WebDriver test failure -->
	<screenshotOnFailure>true</screenshotOnFailure>
</bumblebee>
```

### JUnit Configuration
To enable screenshot capturing, add an instance of ==com.agiletestware.bumblebee.annotations.webdriver.BumblebeeRule== class as a [JUnit rule](https://github.com/junit-team/junit4/wiki/rules) to the test class (just mark BumblebeeRule instance with ==org.junit.Rule== annotation) and invoke the ==setTakeScreenshotOnFail(true)== method.

Bumblebee also provides ==com.agiletestware.bumblebee.annotations.webdriver.BumblebeeRuleBuilder== builder class for the creation and configuration of BumblebeeRule instance.

JUnit Example: Screenshot capturing with BumblebeeRule:

``` java
import java.io.File;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;
import com.agiletestware.bumblebee.annotations.Bumblebee;
import com.agiletestware.bumblebee.annotations.webdriver.BumblebeeRule;
import com.agiletestware.bumblebee.annotations.webdriver.BumblebeeRuleBuilder;

@Bumblebee(testplan = "Subject\\webdriver", testlab = "Root\\webdriver", testset = "screenshot")
public class WebDriverTest {

	@Rule
	public final BumblebeeRule rule = new BumblebeeRuleBuilder(() -> new ChromeDriver())
			.setTakeScreenshotOnFailure(true).build();

	@BeforeClass
	public static void beforeClass() {
		System.setProperty("webdriver.chrome.driver", new File("chromedriver.exe").getAbsolutePath());
	}

	@Test
	public void testPass() {
		rule.getWebDriver().get("http://agiletestware.com");
		Assert.assertEquals("Agiletestware - Software for QA and Development Tools", rule.getWebDriver().getTitle());
	}

	@Test
	public void testFail() {
		rule.getWebDriver().get("http://agiletestware.com");
		Assert.assertEquals("Something wrong", rule.getWebDriver().getTitle());
	}

}

```

Results in HP ALM:  

[![Result in HP ALM](../img/qa-frameworks/selenium-screenshots-1.png "Result in HP ALM")](../img/qa-frameworks/selenium-screenshots-1.png "Result in HP ALM")

!!! note
    You don't need to quit WebDriver instance after each test explicitly. It is automatically quit by BumblebeeRule. If you don't wish to quit driver after each test, simply set a custom ==com.agiletestware.bumblebee.annotations.webdriver.WebDriverCloseBehavior== with ==setWebDriverCloseBehavior()== method of builder.


### TestNG Configuration

To capture screenshots on failed test, add ==com.agiletestware.bumblebee.annotations.testng.BumblebeeTestNGListener== listener to the POM file. This is in addition to ==com.agiletestware.bumblebee.annotations.testng.BumblebeeTestNGReporter==.    

Example pom.xml:

```xml
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>com.agiletestware</groupId>
	<artifactId>bumblebee-demo</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<name>Dummy tests for bumblebee project</name>
	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.1</version>
				<inherited>true</inherited>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>2.19</version>
				<configuration>
					<properties>
						<property>
							<name>listener</name>
							<value>com.agiletestware.bumblebee.annotations.testng.BumblebeeTestNGReporter,com.agiletestware.bumblebee.annotations.testng.BumblebeeTestNGListener</value>
						</property>
					</properties>
				</configuration>
			</plugin>
		</plugins>
	</build>
	<dependencies>
		<dependency>
			<groupId>org.testng</groupId>
			<artifactId>testng</artifactId>
			<version>6.14.3</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>com.agiletestware</groupId>
			<artifactId>bumblebee-annotations</artifactId>
			<version>0.1.4</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.seleniumhq.selenium</groupId>
			<artifactId>selenium-server</artifactId>
			<version>3.0.1</version>
		</dependency>
	</dependencies>
	<repositories>
		<repository>
			<id>nexus.agiletestware.com</id>
			<url>https://nexus.agiletestware.com/repository/maven-public</url>
		</repository>		
	</repositories>
</project>
```



**Implement com.agiletestware.bumblebee.annotations.webdriver.WebDriverTest interface**

It is required for a test class to implement ==com.agiletestware.bumblebee.annotations.webdriver.WebDriverTest== interface which contains only one method - ==getWebDriver()==. This method returns a [RemoteWebDriver](https://seleniumhq.github.io/selenium/docs/api/java/org/openqa/selenium/remote/RemoteWebDriver.html) instance which is used by test.

```java
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.agiletestware.bumblebee.annotations.BooleanValue;
import com.agiletestware.bumblebee.annotations.Bumblebee;
import com.agiletestware.bumblebee.annotations.webdriver.WebDriverTest;

@Bumblebee(testplan = "Subject\\webdriver", testlab = "Root\\webdriver", testset = "screenshot")
public class RemoteWebDriverTest implements WebDriverTest {

	private static final String USERNAME = "userName";
	private static final String AUTOMATE_KEY = "key";
	private static final String URL = "http://" + USERNAME + ":" +
			AUTOMATE_KEY + "@ondemand.saucelabs.com:80/wd/hub";
	private RemoteWebDriver webDriver;

	@BeforeMethod
	public void setUp() {		
		try {
			webDriver = new RemoteWebDriver(new java.net.URL(URL), DesiredCapabilities.firefox());
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void testOne() {
		webDriver.get("http://agiletestware.com");
		Assert.assertEquals(webDriver.getTitle(), "Agiletestware - Software for QA and Development Tools");
	}

	@Test
	public void testTwo() {
		webDriver.get("http://agiletestware.com");
		Assert.assertEquals(webDriver.getTitle(), "Something wrong");
	}

	@AfterMethod
	public void after() {
		webDriver.quit();
	}

	@Override
	public RemoteWebDriver getWebDriver() {
		return webDriver;
	}
}
```

Results in HP ALM:  
[![Result in HP ALM](../img/qa-frameworks/selenium-screenshots-testng-1.png "Result in HP ALM")](../img/qa-frameworks/selenium-screenshots-testng-1.png "Result in HP ALM")

## Adding arbitrary attachments (since 0.0.7)
To add an arbitrary attachment to a run in ALM, Bumblebee provides a static utility class ==com.agiletestware.bumblebee.annotations.util.CurrentTest== with a set of methods:

*   addAttachment(File) - add a single file
*	addAttachment(String, InputStream) - add a single attachment from a stream
*   addAttachments(List<File>) - add list of files

Here is an example of adding attachments:

``` java
@Bumblebee(testlab = "Root\\junit_simple", testset = "class_annotations", testplan = "Subject\\junit_simple")
public class SomePassSomeFailTest {

	@Test
	public void test1() {
		CurrentTest.addAttachment(new File("test.log"));
	}
}
```

Results in ALM:

[![Result in HP ALM](../img/qa-frameworks/JUnit-attachment-1.png "Results in HP ALM")](../img/qa-frameworks/JUnit-attachment-1.png)  

## Adding arbitrary test steps (since 0.1.1)
To add additional test steps to test in ALM, Bumblebee provides ==com.agiletestware.bumblebee.annotations.util.CurrentTest.addStep(String)== method which returns a builder object which provides convenient way to set attributes of step.

``` java
@Bumblebee(testlab = "Root\\junit_simple", testset = "class_annotations", testplan = "Subject\\junit_simple")
public class SomePassSomeFailTest {

	@Test
	public void test_additional_step() {
		final Step step = CurrentTest.addStep("Additional step")
				.description("description")
				.expected("expected")
				.actual("actual")
				.status(Status.FAILED)
				.build();
		step.addAttachment(new File("test.log"));
	}
}
```

Results in ALM:

[![Result in HP ALM](../img/qa-frameworks/JUnit-step-1.png "Results in HP ALM")](../img/qa-frameworks/JUnit-step-1.png)  