title: Sauce Labs and HP ALM Integration Guide - Bumblebee Documentation
description: Bumblebee provides a simple way to integrate HP ALM with Sauce Labs. See the full guide here.

# Integrating with [Sauce Labs](https://saucelabs.com/)

[Sauce Labs](https://saucelabs.com/) is a popular cloud-hosted, web and mobile application automated testing tool which is being used by many companies.  
Bumblebee provides a simple way to integrate HP ALM with Sauce Labs and allows users to:

*	Fetch test metadata from Sauce Labs and publish it in HP ALM
*	Update actual status of a test in Sauce Labs

## Sauce Labs Connection Details
There are two ways to configure SauceLabs connection details

**1. Bumblebee Config File**
Add the following element in the bumblebee_config.xml file:

```xml
<remoteTestTool>
	<saucelabs>		
		<user>username</user>
		<apiKey>apikey123</apiKey>
	</saucelabs>
</remoteTestTool>
```
where ==user== is your SauceLabs username and ==apiKey== is your SauceLab user's API key. By default, Bumblebee will use https://saucelabs.com as a base URL for Sauce Labs REST API calls. It can be changed by adding ==url== element int ==saucelabs== element:

```xml
<remoteTestTool>
	<saucelabs>
		<url>https://someotherurl</url>
		<user>username</user>
		<apiKey>apikey123</apiKey>
	</saucelabs>
</remoteTestTool>
```

Example:

``` xml
<?xml version="1.0"?>
<bumblebee>
    <!-- URL of the Bumblebee Server -->
    <bumblebee_url>http://server_name:port/bumblebee</bumblebee_url>
    <!-- URL of HP ALM Server -->
    <alm_url>http://server_name:port/qcbin</alm_url>
    <!-- Name of HP ALM User -->
    <alm_user>qcuser</alm_user>
    <!-- Encrypted password: please use http://server_name:port/bumblebee/password/encrypt to encrypt your plain text password  -->
    <alm_encrypted_pass>fd4OMOXLJjkMR6e64RJh3Q==</alm_encrypted_pass>
    <!-- HP ALM Domain -->
    <alm_domain>DEFAULT</alm_domain>
    <!-- HP ALM Project -->
    <alm_project>annotations_demo</alm_project>
    <!-- Asynchronous (offline) update -->
    <async_update>true</async_update>
    <remoteTestTool>
		<saucelabs>			
			<user>username</user>
			<apiKey>apikey123</apiKey>
		</saucelabs>
	</remoteTestTool>
</bumblebee>
```

**2. JAVA System Properties**
There are three JAVA system properties for setting Sauce Labs connection details:

*	**bumblebee.saucelabs_url:**  Sauce Labs base URL. If this not set, Bumblebee will use the value from XML configuration or https://saucelabs.com
*	**bumblebee.saucelabs_user:** Sauce Labs username.
*	**bumblebee.saucelabs_apiKey:** Sauce Labs username's API key.

Values passed as system properties override values in XML configuration.

## JUnit Configuration

In order to allow Bumblebee send Sauce Labs test data, add an instance of ==com.agiletestware.bumblebee.annotations.webdriver.BumblebeeRule== to the test class.  

In addition, test methods must use WebDriver object, associated with that BumblebeeRule.  
It can be obtained by calling ==BumblebeeRule.getWebDriver()== method.  

==com.agiletestware.bumblebee.annotations.webdriver.BumblebeeRuleBuilder== can be used for easier creation of BumblebeeRule.

Example: How to use JUnit BumblebeeRule for Sauce Labs integration:

```java
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.agiletestware.bumblebee.annotations.Bumblebee;
import com.agiletestware.bumblebee.annotations.webdriver.BumblebeeRule;
import com.agiletestware.bumblebee.annotations.webdriver.BumblebeeRuleBuilder;

@Bumblebee(testplan = "Subject\\webdriver", testlab = "Root\\webdriver", testset = "saucelabs")
public class WebDriverTest {
	private static final String USERNAME = "user";
	private static final String AUTOMATE_KEY = "api-key";
	private static final String URL = "http://" + USERNAME + ":" + AUTOMATE_KEY + "@ondemand.saucelabs.com:80/wd/hub";

	@Rule
	public BumblebeeRule rule = new BumblebeeRuleBuilder(() -> {
		try {
			return new RemoteWebDriver(new java.net.URL(URL), DesiredCapabilities.firefox());
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}).setTakeScreenshotOnFailure(true).build();

	@Test
	public void testPass() {
		rule.getWebDriver().get("http://agiletestware.com");
		Assert.assertEquals("Agiletestware - Software for QA and Development Tools", rule.getWebDriver().getTitle());
	}

	@Test
	public void testFail() {
		rule.getWebDriver().get("http://agiletestware.com");
		Assert.assertEquals("Something wrong", rule.getWebDriver().getTitle());
	}

}

```

Here are results in HP ALM after running tests with Maven:

[![Result in HP ALM](../img/qa-frameworks/saucelabs-1.png "Result in HP ALM")](../img/qa-frameworks/saucelabs-1.png "Result in HP ALM")

Results in Sauce Labs:

[![Result in HP ALM](../img/qa-frameworks/saucelabs-2.png "Result in HP ALM")](../img/qa-frameworks/saucelabs-2.png "Result in HP ALM")

## TestNG Configuration

To integrate with Sauce Labs, add ==com.agiletestware.bumblebee.annotations.testng.BumblebeeTestNGListener== listener to the project XML file.   

Here is an example of a pom.xml file:

```xml
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>com.agiletestware</groupId>
	<artifactId>bumblebee-demo</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<name>Dummy tests for bumblebee project</name>
	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.1</version>
				<inherited>true</inherited>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>2.19</version>
				<configuration>
					<properties>
						<property>
							<name>listener</name>
							<value>com.agiletestware.bumblebee.annotations.testng.BumblebeeTestNGReporter,com.agiletestware.bumblebee.annotations.testng.BumblebeeTestNGListener</value>
						</property>
					</properties>
				</configuration>
			</plugin>
		</plugins>
	</build>
	<dependencies>
		<dependency>
			<groupId>org.testng</groupId>
			<artifactId>testng</artifactId>
			<version>6.14.3</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>com.agiletestware</groupId>
			<artifactId>bumblebee-annotations</artifactId>
			<version>0.1.4</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.seleniumhq.selenium</groupId>
			<artifactId>selenium-server</artifactId>
			<version>3.0.1</version>
		</dependency>
	</dependencies>
	<repositories>
		<repository>
			<id>nexus.agiletestware.com</id>
			<url>https://nexus.agiletestware.com/repository/maven-public</url>
		</repository>
	</repositories>
</project>
```
**Implementing com.agiletestware.bumblebee.annotations.webdriver.WebDriverTest interface**

It is required for a test class to implement ==com.agiletestware.bumblebee.annotations.webdriver.WebDriverTest== interface which contains only one method - ==getWebDriver()==.  
This method returns a [RemoteWebDriver](https://seleniumhq.github.io/selenium/docs/api/java/org/openqa/selenium/remote/RemoteWebDriver.html) instance which is used by test.

Example: TestNG SauceLabs integration:

```java
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.agiletestware.bumblebee.annotations.BooleanValue;
import com.agiletestware.bumblebee.annotations.Bumblebee;
import com.agiletestware.bumblebee.annotations.webdriver.WebDriverTest;

@Bumblebee(testplan = "Subject\\webdriver", testlab = "Root\\webdriver", testset = "saucelabs")
public class RemoteWebDriverTest implements WebDriverTest {

	private static final String USERNAME = "userName";
	private static final String AUTOMATE_KEY = "key";
	private static final String URL = "http://" + USERNAME + ":" +
			AUTOMATE_KEY + "@ondemand.saucelabs.com:80/wd/hub";
	private RemoteWebDriver webDriver;

	@BeforeMethod
	public void setUp() {		
		try {
			webDriver = new RemoteWebDriver(new java.net.URL(URL), DesiredCapabilities.firefox());
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void testOne() {
		webDriver.get("http://agiletestware.com");
		Assert.assertEquals(webDriver.getTitle(), "Agiletestware - Software for QA and Development Tools");
	}

	@Test
	public void testTwo() {
		webDriver.get("http://agiletestware.com");
		Assert.assertEquals(webDriver.getTitle(), "Something wrong");
	}

	@AfterMethod
	public void after() {
		webDriver.quit();
	}

	@Override
	public RemoteWebDriver getWebDriver() {
		return webDriver;
	}
}
```

Here are results in HP ALM after running tests with Maven:

[![Result in HP ALM](../img/qa-frameworks/saucelabs-testng-1.png "Result in HP ALM")](../img/qa-frameworks/saucelabs-testng-1.png "Result in HP ALM")

Results in Sauce Labs:

[![Result in HP ALM](../img/qa-frameworks/saucelabs-testng-2.png "Result in HP ALM")](../img/qa-frameworks/saucelabs-testng-1.png "Result in HP ALM")

## Disabling updating of status in Sauce Labs (since version 0.0.6)
To disable BrowserStack status update, just add `<updateStatus>false</updateStatus>` element into `<saucelabs>` element:

```xml
<remoteTestTool>
	<saucelabs>		
		<user>username</user>
		<apiKey>apikey123</apiKey>
		<updateStatus>false</updateStatus>
	</saucelabs>
</remoteTestTool>
```

The following log message will be added to log:  `Bumblebee: Updating status in Sauce Labs is disabled in configuration --> skipping Sauce Labs status update`
