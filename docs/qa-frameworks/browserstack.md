title: BrowserStack and HP ALM Integration Guide - Bumblebee Documentation
description: Bumblebee provides a simple way to integrate HP ALM with BrowserStack. See how we do it in this guide.

# Integrating with [BrowserStack](https://www.browserstack.com)

Bumblebee provides a simple way to integrate HP ALM with [BrowserStack](https://www.browserstack.com) and allows users to:

*	Fetch test metadata from BrowserStack and publish it in HP ALM
*	Update actual status of a test in BrowserStack

## BrowserStack Connection Details
There are two ways to configure BrowserStack connection details

**1. Bumblebee Config File**
Add the following element in the bumblebee_config.xml file:


```xml
<remoteTestTool>
	<browserstack>		
		<user>username</user>
		<apiKey>apikey123</apiKey>
	</browserstack>
</remoteTestTool>
```
where ==user== is the BrowserStack username and ==apiKey== is BrowserStack user's API key.  
By default, Bumblebee will use https://www.browserstack.com as a base URL for BrowserStack REST API calls. If needed, it can be changed by adding ==url== element int ==browserstack== element:

```xml
<remoteTestTool>
	<browserstack>
		<url>https://someotherurl</url>
		<user>username</user>
		<apiKey>apikey123</apiKey>
	</browserstack>
</remoteTestTool>
```

Example:

``` xml
<?xml version="1.0"?>
<bumblebee>
    <!-- URL of the Bumblebee Server -->
    <bumblebee_url>http://server_name:port/bumblebee</bumblebee_url>
    <!-- URL of HP ALM Server -->
    <alm_url>http://server_name:port/qcbin</alm_url>
    <!-- Name of HP ALM User -->
    <alm_user>qcuser</alm_user>
    <!-- Encrypted password: please use http://server_name:port/bumblebee/password/encrypt to encrypt your plain text password  -->
    <alm_encrypted_pass>fd4OMOXLJjkMR6e64RJh3Q==</alm_encrypted_pass>
    <!-- HP ALM Domain -->
    <alm_domain>DEFAULT</alm_domain>
    <!-- HP ALM Project -->
    <alm_project>annotations_demo</alm_project>
    <!-- Asynchronous (offline) update -->
    <async_update>true</async_update>
    <remoteTestTool>
		<browserstack>			
			<user>username</user>
			<apiKey>apikey123</apiKey>
		</browserstack>
	</remoteTestTool>
</bumblebee>
```

**2. JAVA System Properties**
There are three JAVA system properties for setting BrowserStack connection details:

*	**bumblebee.browserstack_url:** BrowserStack base URL. If this not set, Bumblebee will use the value from XML configuration or https://www.browserstack.com.
*	**bumblebee.browserstack_user:** BrowserStack username.
*	**bumblebee.browserstack_apiKey:** BrowserStack API key.

Values passed as system properties override values in XML configuration.

## JUnit Configuration

In order to allow Bumblebee send BrowserStack test data, add an instance of ==com.agiletestware.bumblebee.annotations.webdriver.BumblebeeRule== to the test class.  

In addition, test methods must use WebDriver object, associated with that BumblebeeRule.  
It can be obtained by calling ==BumblebeeRule.getWebDriver()== method.  

==com.agiletestware.bumblebee.annotations.webdriver.BumblebeeRuleBuilder== can be used for easier creation of BumblebeeRule.

Example: How to use JUnit BumblebeeRule for BrowserStack integration:


```java
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.agiletestware.bumblebee.annotations.Bumblebee;
import com.agiletestware.bumblebee.annotations.webdriver.BumblebeeRule;
import com.agiletestware.bumblebee.annotations.webdriver.BumblebeeRuleBuilder;

@Bumblebee(testplan = "Subject\\webdriver", testlab = "Root\\webdriver", testset = "browserstack")
public class WebDriverTest {
	private static final String USERNAME = "user";
	private static final String AUTOMATE_KEY = "api-key";
	private static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";

	@Rule
	public BumblebeeRule rule = new BumblebeeRuleBuilder(() -> {
		try {
			return new RemoteWebDriver(new java.net.URL(URL), DesiredCapabilities.firefox());
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}).setTakeScreenshotOnFailure(true).build();

	@Test
	public void testPass() {
		rule.getWebDriver().get("http://agiletestware.com");
		Assert.assertEquals("Agiletestware - Software for QA and Development Tools", rule.getWebDriver().getTitle());
	}

	@Test
	public void testFail() {
		rule.getWebDriver().get("http://agiletestware.com");
		Assert.assertEquals("Something wrong", rule.getWebDriver().getTitle());
	}

}

```

Here are results in HP ALM after running tests with Maven:

[![Result in HP ALM](../img/qa-frameworks/browserstack-1.png "Result in HP ALM")](../img/qa-frameworks/browserstack-1.png "Result in HP ALM")

Results in BrowserStack:

[![Result in HP ALM](../img/qa-frameworks/browserstack-2.png "Result in HP ALM")](../img/qa-frameworks/browserstack-2.png "Result in HP ALM")

## TestNG Configuration

To integrate with BrowserStack, add ==com.agiletestware.bumblebee.annotations.testng.BumblebeeTestNGListener== listener to the project XML file.   

Here is an example of a pom.xml file:

```xml
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>com.agiletestware</groupId>
	<artifactId>bumblebee-demo</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<name>Dummy tests for bumblebee project</name>
	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.1</version>
				<inherited>true</inherited>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>2.19</version>
				<configuration>
					<properties>
						<property>
							<name>listener</name>
							<value>com.agiletestware.bumblebee.annotations.testng.BumblebeeTestNGReporter,com.agiletestware.bumblebee.annotations.testng.BumblebeeTestNGListener</value>
						</property>
					</properties>
				</configuration>
			</plugin>
		</plugins>
	</build>
	<dependencies>
		<dependency>
			<groupId>org.testng</groupId>
			<artifactId>testng</artifactId>
			<version>6.14.3</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>com.agiletestware</groupId>
			<artifactId>bumblebee-annotations</artifactId>
			<version>0.1.4</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.seleniumhq.selenium</groupId>
			<artifactId>selenium-server</artifactId>
			<version>3.0.1</version>
		</dependency>
	</dependencies>
	<repositories>
		<repository>
			<id>nexus.agiletestware.com</id>
			<url>https://nexus.agiletestware.com/repository/maven-public</url>
		</repository>
	</repositories>
</project>
```

**Implementing com.agiletestware.bumblebee.annotations.webdriver.WebDriverTest interface**

It is required for a test class to implement ==com.agiletestware.bumblebee.annotations.webdriver.WebDriverTest== interface which contains only one method - ==getWebDriver()==.  
This method returns a [RemoteWebDriver](https://seleniumhq.github.io/selenium/docs/api/java/org/openqa/selenium/remote/RemoteWebDriver.html) instance which is used by test.

Example: TestNG SauceLabs integration:

```java
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.agiletestware.bumblebee.annotations.BooleanValue;
import com.agiletestware.bumblebee.annotations.Bumblebee;
import com.agiletestware.bumblebee.annotations.webdriver.WebDriverTest;

@Bumblebee(testplan = "Subject\\webdriver", testlab = "Root\\webdriver", testset = "browserstack")
public class RemoteWebDriverTest implements WebDriverTest {

	private static final String USERNAME = "username";
	private static final String AUTOMATE_KEY = "api-key";
	private static final String URL = "https://" + USERNAME + ":" +
			AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
	private RemoteWebDriver webDriver;

	@BeforeMethod
	public void setUp() {		
		try {
			webDriver = new RemoteWebDriver(new java.net.URL(URL), DesiredCapabilities.firefox());
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void testOne() {
		webDriver.get("http://agiletestware.com");
		Assert.assertEquals(webDriver.getTitle(), "Agiletestware - Software for QA and Development Tools");
	}

	@Test
	public void testTwo() {
		webDriver.get("http://agiletestware.com");
		Assert.assertEquals(webDriver.getTitle(), "Something wrong");
	}

	@AfterMethod
	public void after() {
		webDriver.quit();
	}

	@Override
	public RemoteWebDriver getWebDriver() {
		return webDriver;
	}
}
```

Here are results in HP ALM after running tests with Maven:

[![Result in HP ALM](../img/qa-frameworks/browserstack-testng-1.png "Result in HP ALM")](../img/qa-frameworks/browserstack-1.png "Result in HP ALM")

Results in BrowserStack:

[![Result in HP ALM](../img/qa-frameworks/browserstack-testng-2.png "Result in HP ALM")](../img/qa-frameworks/browserstack-2.png "Result in HP ALM")

## Disabling updating of status in BrowserStack (since version 0.0.6)
To disable BrowserStack status update, just add `<updateStatus>false</updateStatus>` element into `<browserstack>` element:

```xml
<remoteTestTool>
	<browserstack>		
		<user>username</user>
		<apiKey>apikey123</apiKey>
		<updateStatus>false</updateStatus>
	</browserstack>
</remoteTestTool>
```

The following log message will be added to log:  `Bumblebee: Updating status in BrowserStack is disabled in configuration --> skipping BrowserStack status update`
