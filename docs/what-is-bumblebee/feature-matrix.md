title: Bumblebee Feature Matrix -  Enterprise vs. Basic Edition
description: Bumblebee has two edition. Enterprise and Basic. This document explains the differences between the two.

# Bumblebeee Feature Matrix

!!! success "Bumblebee Basic Edition vs. Bumblebee Enterprise Edition"
    Bumblebee has two license offerings. Enterprise and Basic. This document explains the differences between the two edition.

| Feature / Capability | Bumblebee Basic Edition | Bumblebee Enterprise Edition |
| ------------- | ------------- | ------------- |
| Email Support  |      ![ok](../img/setup/green-ok.png)  |      ![ok](../img/setup/green-ok.png) |
| Phone Support  |  ![ok](../img/setup/cross.png) |      ![ok](../img/setup/green-ok.png) |
| Screen-sharing Support  |      ![ok](../img/setup/cross.png)  |      ![ok](../img/setup/green-ok.png) |
| SLA  | Best Effort  | 24-48 hours |
| Free Upgrade  |      ![ok](../img/setup/green-ok.png)  |      ![ok](../img/setup/green-ok.png) |
| Number of Users/Machines  | 5 users/machines  | Unlimited users/machines |
| Number of Tests uploads  | 500 tests/day  | Unlimited tests/day |
| Defect Management Integration | ![ok](../img/setup/cross.png)  |      ![ok](../img/setup/green-ok.png) - Coming Soon |
| Priority Feature Request |  ![ok](../img/setup/cross.png)|      ![ok](../img/setup/green-ok.png) |
| Priority Bug Fixes | ![ok](../img/setup/cross.png) |      ![ok](../img/setup/green-ok.png) |
