title: Architecture - Bumblebee Documentation
description: Bumblebee Architecture & components

Architecture
============

[![bumblebee architecture](../img/what_is_bumblebee/bumblebee-arch.png)](../img/what_is_bumblebee/bumblebee-arch.png)


Bumblebee is a web-service that runs inside your network. Bumblebee has two components

==**Bumblebee Server**:== 
This is the main component of Bumblebee. All communication to HP ALM is carried out via this layer over a REST API

==**Bumblebee Integration Tools**:==
Bumblebee comes with a set of tools to help you implement any HP ALM workflow within 5-10 minutes. 

---

Here are some of the Bumblebee tools that can help you implement all kinds of workflows within HP ALM.

==**Ingrate CI applications with HP ALM:**==

!!! tip "Jenkins Integration"
    Use the Bumblebee Jenkins plugin to automatically publish tests results from Jenkins to HP ALM. The Jenkins plugin can also schedule tests in ALM, run UFT tests, run Performance Center tests, LeanFT, and to run them locally on the Jenkins slave or remotely on HP ALM.

!!! note "Bamboo Integration"
    Use the Bumblebee Bamboo addon to automatically publish tests results from Bamboo to HP ALM. The Bamboo addon can also schedule tests in ALM, run UFT tests, run Performance Center tests, LeanFT, and to run them locally on the Bamboo agent or remotely on HP ALM.

!!! summary "Teamcity Integration"
    Use the Bumblebee Teamcity plugin to automatically publish tests results from Teamcity to HP ALM. The Teamcity plugin can also schedule tests in ALM, run UFT tests, run Performance Center tests, LeanFT, and to run them locally on the Teamcity agent or remotely on HP ALM.

==**Integrate JAVA based Test Frameworks with HP ALM:**==
Bumblebee provides custom annotations packages for TestNG and JUnit. These annotations packages can be used to integrate Selenium WebDriver, Appium, Cucumber, Robotium, and many more tools with HP ALM without writing any complex code.

!!! tip "JUnit annotations"
    Import Bumblebee dependencies in your java project and simply annotate junit tests with bumblebee annotations to map junit tests to HP test plan, testlab, testet, requirements, and much more.
	
!!! note "testNG annotations"
    Import Bumblebee dependencies in your java project and simply annotate testNG tests with bumblebee annotations to map testNG tests to HP test plan, testlab, testet, requirements, and much more.

!!! summary "Bumblebee REST API"
    Don't like any of our tools but still need to integrate some workflow with HP ALM? Bumblebee's REST API allows users to create their own custom integrations with HP ALM.