title: Release Notes - Bumblebee Documentation
description: Bumblebee Release Notes, bugs and fixes


Release Notes
=============
**Bumblebee version: 5.5**

	[BUM-136] - Add 500 tests/day limitation for non-enterprise users
	[BUM-359] - Bumblebee annotations: Pass real execution date and time (since bumblebee-annotations-0.1.4)
	[BUM-360] - Bumblebee annotations: Pass test duration time to ALM (since bumblebee-annotations-0.1.4)
	[BUM-361] - BUGFIX: Serenity report parser: Screenshots for nested steps are not exported into ALM
	[BUM-362] - BUGFIX: TestNG report parser: Bumblebee processes only the last 'test' element in the report
	[BUM-363] - BUGFIX: Bumblebee annotations: Bumblebee stops test execution if screenshot capturing fails
	[BUM-364] - BUGFIX: TestNG report parser: Bumblebee fails if report root is 'suite'	

**Bumblebee version: 5.4**
	
	[BUM-356] - Password encryption produces incorrect result when some special characters are used
	[BUM-357] - Version does not show up when license is not valid
	[BUM-358] - Bumblebee Annotations throws error when almid is set and testplan is not set

**Bumblebee version: 5.3**
	
	[BUM-303] - Add support for Cucumber formats into new server (REST API based)
	[BUM-306] - Update requirements in /alm	(REST API based)
	[BUM-348] - Add support for providing values for test-instance's fields
	
**Bumblebee version: 5.2**

	[BUM-337] - BUGFIX: Fix inability to pass values for ALM required fields (e.g. TS_STATUS)
	[BUM-340] - BUGFIX: Server throws error when uploading results from bumblebee-annotations when annotation is on test method level, contains almid and does not contain testplan	

**Bumblebee version: 5.1**

	[BUM-302] - New server: Add version number
	[BUM-315] - Fetch results from ALM: Get step's expected and actual
	[BUM-322] - Add support for JBehave XML
	[BUM-323] - Fix NPE in Serenity parser when parent step does not contain result
	[BUM-324] - Bumblebee Server for Linux does not work with JRE 11
	[BUM-330] - Logs are not displayed under some conditions
	[BUM-332] - Annotations: Attachments are not added on run level

**Bumblebee version: 5.0**
	
	- Bumblebee Server was reworked to use ALM REST API instead of ALM OTA API.  
	- The first release of Unix/Linux version

**Bumblebee version: 4.1.9**

New Features:

	[BUM-242] - Add support of TLS for maintaining secure HTTP connection

Bugs:

	[BUM-291] - Fix connectivity issues with Performance Center using proxy server
	[BUM-296] - Fix test set runner issue: "Your Quality Center session has been disconnected" 

**Bumblebee version: 4.1.8**

New Features:

	[BUM-98] - Upload attachments into ALM from bumblebee-annotation package
	[BUM-194] - New plugin for IBM UrbanCode Build
	[BUM-204] - CI Plugins: Add "Skip connectivity diagnostic" checkbox to admin page
	[BUM-206] - NUnit: Add custom test steps programmatically
	[BUM-211] - Analyze LoadRunner result and fail test if there are failures
	[BUM-228] - LR Analysis: Return detailed error message for failed transaction
	[BUM-232] - LR Analysis: Fail build only if transaction fails
	[BUM-238] - Add support for JBehave reports
	
Bugs:

	[BUM-234] - Bumblebee Annotations: test methods as steps - TestNG provides incorrect order of tests for reporting
	[BUM-239] - Fix various Serenity bugs
	[BUM-240] - Annotations: TestNG: Skipped tests are not exported to ALM
	[BUM-241] - Annotations: TestNG datadriven test and hasSteps=true produces incorrect result in ALM
	[BUM-217] - OrientDB: java.lang.OutOfMemoryError: Direct buffer memory
	[BUM-231] - LR Analysis: NPE
	[BUM-235] - CI Plugins: Global config page: password containing special symbols ",{,} cause error because query param is not encrypted
	

**Bumblebee version: 4.1.7**

	[BUM-186] - Add support for Serenity reports
	[BUM-196] - CI Plugins: Override user name on a particular PC task
	[BUM-198] - TeamCity: Support protractor-jasmine reports
	[BUM-199] - Bamboo: Support protractor-jasmine reports
	[BUM-205] - Remove event tracking


**Bumblebee version: 4.1.6**

New Features:
	 
    [BUM-73] - Secure Bumblebee Server configuration pages
    [BUM-100] - Add NuGet package to support integration with .NET and NUnit    
    [BUM-153] - New UrbanCode Bumblebee plugin
    [BUM-165] - Add /test_results API method for fetching test results from HP ALM
    [BUM-178] - Mappings can be added for all servers/domains/projects
    [BUM-180] - Improve info message on mapping save
    [BUM-187] - Support protractor-jasmine reports in Jenkins plugin
    
      
Bugs:

    [BUM-192] - HTTP error 411 on trying to restart late analyze in Performance Centers
    

**Bumblebee version: 4.1.5**

New Features:

    [BUM-159] - Jenkins Plugin: Pull test results from HP ALM
    [BUM-164] - Bamboo Plugin: Add retry logic in case if collate or analyze phases failed
    [BUM-172] - Provide mappings to set values to test step user fields
    [BUM-167] - Improve error messages in case if field mappings are not correct or not provided
    [BUM-91] - Improve logging in CI Plugins
      
Bugs:

    [BUM-171] - Proxy settings page does not display proxy port if it is greater than 999
    [BUM-173] - Trim names and values of parameters listed in "Custom Properties" property   

**Bumblebee version: 4.1.4**

Bug:

    [BUM-154] - Fix memory leak causing java.lang.OutOfMemoryError


**Bumblebee version: 4.1.3**

New Features:

    [BUM-149] - CI Plugins: TestNG: add parameters for data driven tests into actual field in HP ALM 
      

Bugs:

    [BUM-143] - Jenkins: Fix bulk update logging
    [BUM-146] - CI Plugins: NullPointerException occurs sometimes during uploading TestNG results
    [BUM-148] - CI Plugins: Incorrect status of a test instance for data driven tests
    [BUM-150] - TeamCity: UnsupportedOperationException in emulation mode


**Bumblebee version: 4.1.2**

New Features:

    [BUM-141] - CI Plugins: Adding tests to test set step: Create HP ALM TestSet automatically if it does not exist 
      

Bug:

    [BUM-142] - CI Plugins: Upload TestNG reports: Do not create tests for config methods (marked with @BeforeClass, @BeforeTest, etc... annotations)


**Bumblebee version: 4.1.1**

New Features:

    [BUM-78] - Annotations: Add Sauce Labs support
    [BUM-82] - Annotations: Add BrowserStack support
    [BUM-134] - Annotations: Selenium WebDriver tests - adding screenshots for failed tests automatically
    [BUM-137] - Adjust value in Actual field of a test step
    [BUM-139] - CI Plugins: Add "Bumblebee: Add test to set" step for adding tests to test set in HP ALM

    

Bug:

    [BUM-138] - Run attachments have incorrect name
    

**Bumblebee version: 4.1.0**

New Features:

    [BUM-79] - Annotations: Map tests to ALM requirements
    [BUM-70] - Annotations: Maps test methods to test steps in HP ALM
    [BUM-102] - CI Plugins: Running local UFT tests
    [BUM-107] - CI Plugins: Running Performance Center tests
    [BUM-92] - Show Bumblebee version on the main page
    [BUM-118] - Installer: let user have empty password
    [BUM-86] - VBScript shall not be overwritten if "executable" parameter is empty
    [BUM-87] - Do not set VBScript for bulk update and extended update
    [BUM-97] - Improve look of web pages

    

Bugs:

    [BUM-71] - Bumblebee crashes sometimes when several threads update the same project
    [BUM-76] - Offline queue page: incorrect sorting when documents have different types
    [BUM-109] - CI Plugins: Fix test duration in JUnit report
    [BUM-85] - Test in a TestPlan is updated even if it does not any changes
    [BUM-94] - alm method: NPE when attachment element does not have fileName
    [BUM-95] - If passed model has less steps than test in hp alm update is not triggered
    [BUM-101] - Offline processing queue and offline document pages are not opened in IE because of wrong content type header
    [BUM-106] - TeamCity: Global settings are not refreshed until restart of a server
    [BUM-108] - Offline processing: it sometimes does not show error when error actually occurs
    [BUM-111] - Offline processing page: incorrect sorting
    [BUM-112] - /alm method: Do not update test instance's step 'description' and 'expected' values if preserveSteps=true
    

**Bumblebee version: 4.0.7**

New Features:

    [BUM-72] - Annotations: Add offline processing parameter
    [BUM-74] - Automatic handling of Fast Runs

Bug:

    [BUM-75] - java.lang.IllegalStateException after some time


**Bumblebee version: 4.0.6**

Bug:

    [BUM-63] - Annotations: NullPointerException if testplan is not set


**Bumblebee version: 4.0.5**

New Features:

    [BUM-61] - Add proxy server support to bumblebee server and Jenkins Plugin

Bug:

    [BUM-62] - Bumblebee.exe is not compatible with some .NET framework versions


**Bumblebee version: 4.0.4**

Bug:

    [BUM-58] - Installer does not delete lib folder before installing new version

**Bumblebee version: 4.0.3**

New Features:

    [BUM-26] - message queues for faster and reliable processing
    [BUM-43] - Jenkins, Teamcity, Bamboo plugin - add build step for running tests in a testset

**Bumblebee version: 4.0.2**

New Features:

    [BUM-27] - java annotation package for bumblebee
    [BUM-52] - Bumblebee new API /alm
    [BUM-53] - TestNG Support
    [BUM-56] - Add support of .trx format
    [BUM-57] - Add support of cucumber format
    [BUM-5] - Bamboo plugin
    [BUM-6] - TeamCity plugin

