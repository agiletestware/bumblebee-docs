title: Bumblebee Server Security - Bumblebee Documentation
description: Configuring Bumblebee Server Security

Configure Bumblebee Server Security
======================

Bumblebee provides a basic security for preventing unauthorized access to some configuration pages, so once user tries to access secured page it will be redirected to the login page:

[![Login](../img/setup/login-1.png)](../img/setup/login-1.png)

*	Default user name: admin  
*	Password: admin

## Managing user accounts

To manage users, navigate to **Accounts** tab:

[![Login](../img/setup/users-1.png)](../img/setup/users-1.png)

To create a new user account, use **New Account** button.  

To change user's details, including password - click on **Edit** button.

## Setting up secure connection to Bumblebee Server
Here are the steps to setup a secure connection (HTTPS) to your Bumblebee Server:

*	Generate or purchase an SSL/TLS certificate and generate a Java keystore of PKCS12 type (https://docs.oracle.com/javase/8/docs/technotes/tools/unix/keytool.html)
*	Put your keystore file (e.g. keystoreFile.pfx) into Bumblebee Server installation directory (on Windows, it is usually C:\Program Files\bumblebee)
*	Create a new ==application.yml== file in Bumblebee Server installation directory
*	Add the following configuration into newly created ==application.yml==

```yml
server:
  port: 8443
  ssl:
    key-store: keystoreFile.pfx
    key-store-password: keystorePassword
    keyStoreType: PKCS12
```

, where `keystoreFile.pfx` is the name of your keystore file, `keystorePassword` password for your keystore

*	Windows: Restart `BumbleBee HP ALM Integration Service` 
*	Linux: Navigate to Bumblebee Server installation directory and run `sudo ./shutdown.sh` and `sudo ./startup.sh` 