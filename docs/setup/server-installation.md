title: Server Installation Guide - Bumblebee Documentation
description: How to install and connect your Bumblebee with HP ALM

Server Installation
===================

----

## Requirements

Bumblebee Server works with HP ALM 10, 11, 12, or SAAS

**Hardware requirements:**

* Core duo 1.6 Ghz (or higher) or equivalent compatible processor
* 2 GB RAM minimum
* 500 MB free disk space

**Software requirements:**

* Microsoft Windows 7 SP 1 (or newer) or Microsoft Windows Server 2008 R2 (or newer)

Or:
* Oracle Linux 5.5 or newer
* Red Hat Enterprise Linux 5.5 or newer
* Ubuntu Linux 12.04 LTS or newer
 

### Support for ALM 10 and 11

In order to use Bumblebee Server with HP ALM 10 and 11, please download <a href="https://s3-us-west-2.amazonaws.com/agiletestware-bumblebee/downloads/4.1.9/bumblebee_4.1.9.exe">the latest version of Bumblebee Server which supports an old OTA API</a>  

#### Prerequisites for Bumblebee Server prior 5.0
*	Windows OS
*	Install HP Quality Center Connectivity Add-in from ALM management tools. Typically `http://<alm url>:8080/qcbin/addins.html`


[![alm client](../img/setup/2015-06-04_02h05_05.png)](../img/setup/2015-06-04_02h05_05.png)

---

## Windows

### Installation

Download and run the installer  

[![bumblebee installer](../img/setup/bumblebee-setup-1.png)](../img/setup/bumblebee-setup-1.png)

Accept the EULA

[![bumblebee eula](../img/setup/bumblebee-setup-2.png)](../img/setup/bumblebee-setup-2.png)

Specify the license file that was sent via email.

[![bumblebee license](../img/setup/bumblebee-setup-3.png)](../img/setup/bumblebee-setup-3.png)

Specify installation directory

[![bumblebee directory](../img/setup/bumblebee-setup-4.png)](../img/setup/bumblebee-setup-4.png)

Specify some available port 

[![bumblebee port](../img/setup/bumblebee-setup-5.png)](../img/setup/bumblebee-setup-5.png)

[![bumblebee progress](../img/setup/bumblebee-setup-6.png)](../img/setup/bumblebee-setup-6.png)

[![bumblebee console](../img/setup/bumblebee-setup-10.png)](../img/setup/bumblebee-setup-10.png)

The Bumblebee URL takes you to the admin page, where you can configure custom properties, view logs, view and update license, and view your encrypted password

## Linux

### Prerequisites

* 	Java Runtime Environment 1.8 must be installed and it's location must be added to PATH environment variable. To check if it's set run the following command in terminal `java -version`.

### Installation

The following steps need to be done in order to install and start Bumblebee Server:

*	[Download Bumblebee server archive](https://www.agiletestware.com/bumblebee#download)
*	Unpack bumblebee-unix-<version>.tar.gz into some directory, e.g. /opt
*	Copy your license file into bumblebee directory (e.g. /opt/bumblebee)
*	Run `sudo startup.sh` command from bumblebee folder.

!!! tip	"Port specification"
	By default, Bumblebee Server uses port 8080.  
	To change the port number, provide `-port <number>` command line option: e.g. `sudo startup.sh -port 9999`

## Bumblebee Web Portal
[![bumblebee web](../img/setup/login-1.png)](../img/setup/login-1.png)

The default user name/password: admin/admin

[![bumblebee web](../img/setup/bumblebee-main-page.png)](../img/setup/bumblebee-main-page.png)

----
