title: HP ALM Mappings - Bumblebee Documentation
description: In many HP ALM projects, ALM administrators define custom user fields in project entities such as Test, TestSets, Test Runs, etc. In such a setup, your ALM tests may expose optional and/or mandatory customer fields

Configure ALM Mappings
======================

## - Description
In many HP ALM projects, ALM administrators define custom user fields in project entities such as Test, TestSets, Test Runs, etc. In such a setup, your ALM tests may expose optional and/or mandatory customer fields

[![alm user fields](../img/setup/bumblebee-setup-12.png)](../img/setup/bumblebee-setup-12.png)

## - Configuration
Bumblebee makes it super easy to configure custom user fields using the web based configuration page. Simply go to the ==configure alm mappings. http://[bumblebee server]:port/bumblebee/mappings== page.

If you are not logged in, then you will be redirected to the login screen:

[![login](../img/setup/login-1.png)](../img/setup/login-1.png)

*	Default user name: admin  
*	Password: admin

!!! tip "Changing password"
	Once logged in, you can change the password by navigating to **"Accounts"** tab. See [security configuration](server-security.md) for details.

[![bumblebee config](../img/setup/bumblebee-setup-13.png)](../img/setup/bumblebee-setup-13.png)

Once you are on this page, modify the XML by specifying your ALM project's relevant custom user field mappings and save the mapping. Here is a sample XML mappings for our ALM server

```xml
<!-- Mappings file. -->
<mappings>
    <!-- URL of your ALM server -->
    <url url="http://192.168.0.52:8080/qcbin">
        <!-- ALM domain name -->
        <domain name="DEFAULT">
            <!-- ALM project name -->
            <project name="test_project">
                <!-- mappings for Test -->
                <!-- alm_field - actual name of field in ALM -->
                <!-- label - name of HTTP request query param which value should be mapped into alm_field -->
                <!-- default_value - default value will be used if no HTTP request query parameter was passed.-->
                <!-- Delete it if no default value is needed -->
                <mapping alm_field="TS_USER_01" default_value="value" label="test user 1"/>
                <!-- mappings for TestSet -->
                <mapping alm_field="CY_USER_01" default_value="" label="test set user 1"/>
                <!-- mappings for Run -->
                <mapping alm_field="RN_USER_01" label="run user 1"/>
            </project>
        </domain>
    </url>
</mappings>
```

!!! warning "Required Custom Fields"
    If your ALM project has ==mandatory== custom fields, they must be specified in the XML. Not mapping mandatory fields will cause failtures because HP ALM enforces validaton of these values.
	
==alm_field== and ==label== value can be obtained from your ALM project admin by going to ==Customize --> Project Entities --> [entity type] --> User Fields==

[![alm user config](../img/setup/bumblebee-setup-14.png)](../img/setup/bumblebee-setup-14.png)

## - Specifying common mappings for all urls, domains or projects (since 4.1.6)
If there are many projects with the same mapping configuration, it could be not very convenient to define the same configurations for each and every project.  
Bumblebee can address this issue by allowing ==mapping== elements on any level in XML mappings file:

### - Common mappings for all projects in all ALM instances

```xml
<!-- Mappings file. -->
<mappings>
	<!-- This will be defined for any project -->
	<mapping alm_field="TS_USER_01" default_value="value" label="test user 1"/>
</mappings>"
```

The mapping element can be overridden on url/domain/project level by specifying mapping element with the same value in ==alm_field== attribute.

### - Common mappings for all projects in one HP ALM instance

```xml
<!-- Mappings file. -->
<mappings>
	 <!-- URL of your ALM server -->
    <url url="http://192.168.0.52:8080/qcbin">
    	<!-- This will be defined for any project inside this url element -->
    	<mapping alm_field="TS_USER_01" default_value="value" label="test user 1"/>      
    </url>	
</mappings>"
```

### - Common mappings for all projects in a single HP ALM domain

```xml
<!-- Mappings file. -->
<mappings>
    <!-- URL of your ALM server -->
    <url url="http://192.168.0.52:8080/qcbin">
        <!-- ALM domain name -->
        <domain name="DEFAULT">
        	<!-- This will be defined for any project inside this domain -->
        	<mapping alm_field="TS_USER_01" default_value="value" label="test user 1"/>           
        </domain>
    </url>
</mappings>"
```
