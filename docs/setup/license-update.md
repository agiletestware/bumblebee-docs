title: Bumblebee License Update - Bumblebee Documentation
description: To update the license follow the steps in this guide

Updating license
===================

----

To update the license follow the steps below:

*	Open Bumblebee main page in your browser (http://&lt;bumblebee&#95;url:port&gt;/bumblebee) and click on "License Information" link or navigate directly to http://&lt;bumblebee&#95;server:port&gt;/bumblebee/licenseinfo/view

[![updating license](../img/setup/license-1.png)](../img/setup/license-1.png) 

If you are not logged in, Bumblebee will redirect you to the login screen:  
Default user name/password: admin/admin

[![updating license](../img/setup/login-1.png)](../img/setup/login-1.png)

!!! note "Server security"
	Please see more details about server-security on the [corresponding page](server-security.md)

*	Click on ==Browse== button, select new license file and click on ==Open== button
*	Click ==Upload== button

[![updating license](../img/setup/license-2.png)](../img/setup/license-2.png)