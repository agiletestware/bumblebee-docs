title: POST & HP ALM Integration Guide - Bumblebee Documentation
description: This API method provides the most flexible way of creating/updating test information in HP ALM.

# POST /alm
## Description
This API method provides the most flexible way of creating/updating test information in HP ALM.
It provides the following key features:

*	Create/Update tests in HP ALM TestPlan
*	Create/Update test instances in HP ALM TestLab
*	Add runs with detailed test steps reports (including file attachments) to HP ALM TestLab
*	Pass values for HP ALM custom fields for Test, TestSet and Run instances

## Details
**Method:** POST

**Content-Type:** multipart/form-data

The first part of a multipart request must be a file in XML format with data describing exporting structure.
Please see detailed description of XML file format below.

Other parts contain file attachments which shall be uploaded to HP ALM together with the structure. XML document contains references to attached files, so Bumblebee can recognize where to attach each file (see below).

## XML file format
As described above, the first part of a multipart request must contain a file in XML format containing structure which shall be imported to HP ALM.
Here is an example of XML file:

	<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
	<project url="http://almurl:8080/qcbin" domain="DEFAULT" project="test" user="qcuser" encryptedPassword="fd4OMOXLJjkMR6e64RJh3Q==" mode="FULL" offline="true">
		<suite name="Suite name" testPlan="Subject\Folder1\Folder2" testLab="Root\Folder1\Folder2" testSet="Demo TestSet">
			<case name="API Demo Test" almid="0" description="Test description" status="Failed" preserveSteps="false">
				<step name="Test step 1" description="some description" status="Failed">
					<error>java.lang.AssertionError: test1 failed</error>
					<expected>Expected 3</expected>
				</step>
				<step name="Test step 2" status="Passed"/>
				<param name="test param" value="test value"/>				
				<param name="run param" value="24"/>				
			</case>			
			<param name="test set param" value="test set value"/>			
		</suite>
	</project>
	
### `project` element:

`project` element is the root of the XML structure for uploading data to HP ALM with Bumblebee and it has the following attributes: 

| Attribute name | Description | Example | Required |
|-------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------|----------|
| url | HP ALM URL | http://almurl:8080/qcbin | Yes |
| domain | HP ALM Domain | DEFAULT | Yes |
| project | HP ALM Project | test | Yes |
| user | HP ALM user name | qcuser | Yes |
| encryptedPassword | Encrypted password for HP ALM user (see [/password method docs](password.md)  to learn how to encrypt password) | fd4OMOXLJjkMR6e64RJh3Q== | Yes |
| mode | Update mode, it must contain one of the following values:  <ul><li>TESTPLAN - create/update data in HP ALM TestPlan only (results will not be uploaded to HP ALM TestLab)</li><li>TESTLAB - create/update test results in HP ALM TestLab only</li><li>FULL - create/update tests in HP ALM TestPlan and results in HP ALM TestPlan. Usually this is preferable mode to use.</li></ul> | FULL | Yes |
| offline | Boolean flag which enables/disables asynchronous (offline) update. If it is not set, then default is "false" | true/false | No, default is false |

A project element also has 1 or more child `suite` elements which group a set of tests together.

### `suite` element:
`suite` element represents a set of test and/or test results grouped by HP ALM test plan directory, test lab directory and test set.
Here is the list of available attributes of `suite` element:

| Attribute name | Description | Example | Required |
|----------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------|----------|
| name | The name of suite. It currently has no effect on uploading data to HP ALM, so it can have any value (it appears only in logs). | Suite name | Yes |
| testPlan | Path to TestPlan folder in HP ALM to where tests will be exported. Must start with `Subject\`. If such a folder does not exist, it will be created automatically. | Subject\Folder1\Folder2 | Yes |
| testLab | Path to TestPlan folder in HP ALM to where test results will be exported. Must start with `Root\`. If such a folder does not exist, it will be created automatically. | Root\Folder1\Folder2 | Yes |
| testSet | The name of HP ALM test set to where results will be exported. If it does not exist, it will be created automatically. | Demo TestSet | Yes |

Each suite element may have zero or more `param` elements and 1 or more `case` elements.

### `param` element
`param` element represents a parameter name/value pair passed to HP ALM to set value to HP ALM custom fields of corresponding entity: if it is located inside `suite` element, then it is mapped to HP ALM TestSet.
If it is located inside `case` element, then it is mapped to HP ALM test and run.

Here is the list of available attributes of `param` element:

| Attribute name | Description | Example | Required |
|----------------|---------------------------------------------------------------------------------|------------|----------|
| name | The name of parameter. Parameters must be first set on the [Bumblebee Configure Mappings page](../setup/alm-mappings.md). Use value of `label` attribute of a `mapping` element for a parameter name| param name | Yes |
| value | Value of parameter | value | Yes |

### `case` element
`case` element represents a single test and has the following attributes:

| Attribute name | Description | Example | Required |
|----------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------|-------------------------------------------|
| name | The name of a test in HP ALM. | API Demo Test | Yes |
| description | A description of a test | Test description | No |
| status | Execution status of a test. Must have one of the following values: <ul><li>Passed</li><li>Failed</li><li>Not Completed</li><li> No Run</li><li>N/A</li></ul> | Passed | Yes, if project's mode is TESTLAB or FULL |
| preserveSteps | Whether existing test steps shall be preserved. true - steps will not be overriden, new steps just added to the test false - existing test steps will be deleted and new steps will be created | true or false | No, default value is `false` |
| almid | An ID of a test in HP ALM. If it is set to non-zero, then Bumblebee will try to update and upload results for a test with the given ID and fail if it does not exist. testplan attribute of a project is ignored in such a case | 42 | No |

Each `case` element contains:

*	0 or more `param` elements defining values for test/run custom HP ALM fields
*	1 or more `step` elements

### `step` elements
`step` element represents a single design step in HP ALM and also holds result for step of a test instance in TestLab.

Here is the list of available attributes of `step` element:

| Attribute name | Description | Example | Required |
|----------------|--------------------------------------------------------------------------------------|-----------------------|-------------------------------------------|
| name | The name of a test step in HP ALM. | Test step 1 | Yes |
| description | A description of a test step | Test step description | No |
| status | Execution status of a test. Must have one of the following values: <ul><li>Passed</li><li>Failed</li><li>N/A</li></ul> | Passed | Yes, if project's mode is TESTLAB or FULL |

Each `step` element may also contain the following optional elements:

*	`actual` - value to be set into `Actual` field of a Test Step in HP ALM TestLab
*	`expected` - value to be set into `Expected` field of a Test Step in HP ALM TestLab
*	`error` - additional error message which is set into `Actual` field of a Test Step in HP ALM TestLab together with value from `Actual` element
*	0 or more `attachment` element which represents attachments to test steps and will be attached to a step in HP ALM TestLab

### `attachment` element
Represent a single file attachment to a test step and has the following attributes:

*	fileName - a name of a file how it shall appear in HP ALM.
*	id - a unique identifier (shall be unique within the whole XML document) for the file attachment. It is used to map an attachment stored in some part of multipart HTTP request to a particular test step.

## Uploading file attachments to Test Steps in HP ALM TestLab
To upload a file into test step attachments user needs to do the following:

*	Add an `attachment` element into `step` element and set values to `id` and `fileName` attributes.
*	For each `attachment` element, a separate part shall be added to the HTTP multipart request
*	Content-Disposition HTTP header of a part must have `filename` property which contains the same value as `id` attribute of the corresponding attachment.

Here is the example of HTTP Request:
	
	POST http://localhost:8888/bumblebee/alm HTTP/1.1
	Accept-Encoding: gzip,deflate
	Content-Type: multipart/form-data; boundary="----=_Part_12_1445180340.1485184652622"
	MIME-Version: 1.0
	Content-Length: 84148
	Host: localhost:8888
	Connection: Keep-Alive
	User-Agent: Apache-HttpClient/4.5.2 (Java/1.8.0_102)
	
	
	------=_Part_12_1445180340.1485184652622
	Content-Type: application/xml; name=test_payload.xml
	Content-Transfer-Encoding: binary
	Content-Disposition: form-data; name="test_payload.xml"; filename="test_payload.xml"
	
	<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
	<project url="http://almurl:8080/qcbin" domain="DEFAULT" project="test" user="qcuser" encryptedPassword="fd4OMOXLJjkMR6e64RJh3Q==" mode="FULL" >
		<suite name="Suite name" testPlan="Subject\Folder1\Folder2" testLab="Root\Folder1\Folder2" testSet="Demo TestSet">
			<case name="API Demo Test" almid="0" description="Test description" status="Failed" preserveSteps="false">
				<step name="Test step" description="some description" status="Failed">
					<error>java.lang.AssertionError: test1 failed</error>
					<expected>Expected 3</expected>
					<attachment id="step1_attachment" fileName="Test failure.png"/>
				</step>
				<step name="test2" status="Passed"/>
				<param name="test param" value="test value"/>			
				<param name="run param" value="24"/>				
			</case>		
			<param name="test set param" value="test set value"/>		
		</suite>
	</project>
	------=_Part_12_1445180340.1485184652622
	Content-Type: image/x-png; name=step1_attachment
	Content-Transfer-Encoding: binary
	Content-Disposition: form-data; name="zzz"; filename="step1_attachment"
	
	‰PNG
	... other binary data is omitted for brevity
	
## Response
### HTTP Codes:
*	200 - OK. Check XML response for details of execution.
*	500 - Internal critical server error occurred and no tests were exported. Contains an error message in body.

### Response details
Response object contains upload result for all exported tests:
If all tests were exported successfully, then each `case` element has `exportStatus` is `OK` and also each case has a value in `almid` attribute which points to a test in HP ALM TestPlan:
	
	<?xml version="1.0" encoding="UTF-8"?>
	<project offline="false">
		<suite name="Suite name" testPlan="Subject\Folder1\Folder2" testLab="Root\Folder1\Folder2" testSet="Demo TestSet">
			<case name="API Demo Test" almid="436" exportStatus="OK" preserveSteps="false"/>							
		</suite>
	</project>


Bumblebee does not fail the whole export if it could not upload a single test case, but sets its `exportStatus` to `ERROR` and continue execution:

	HTTP/1.1 200 OK
	Content-Type: application/xml
	Date: Mon, 23 Jan 2017 15:36:41 GMT
	Content-Length: 1220
	
	<?xml version="1.0" encoding="UTF-8"?>
	<project offline="false">
		<suite name="Suite name" testPlan="Subject\Folder1\Folder2" testLab="Root\Folder1\Folder2" testSet="Demo TestSet">
			<case name="API Demo Test" almid="436" exportStatus="OK" preserveSteps="false"/>							
			<case name="API Demo Test 1" almid="0" exportStatus="ERROR" preserveSteps="false">
				<executionError>Either mappings XML file does not contain mapping or request does not contain value for user field TS_USER_01 (TS_USER_01) which is required field in your ALM instance. Please check mappings at /mappings page and your request
	Stack trace:
	com.agiletestware.alm.util.AlmMappingChecker.findAlmMappingOrFail(AlmMappingChecker.java:56)
	com.agiletestware.alm.util.AlmMappingChecker.assertAlmMappings(AlmMappingChecker.java:27)
	com.agiletestware.alm.export.helpers.DefaultTestCaseExporter.assertAlmMappings(DefaultTestCaseExporter.java:74)
	com.agiletestware.alm.export.helpers.DefaultTestCaseExporter.exportTestCase(DefaultTestCaseExporter.java:50)
	com.agiletestware.bumblebee.ws.alm.BumblebeeExporter.exportTestCase(BumblebeeExporter.java:70)</executionError>			
			</case>
		</suite>
	</project>
		
