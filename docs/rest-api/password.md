title: GET /password - Bumblebee Documentation
description: Password API returns the encrypted password for your plain text password used for the ALM user. All Bumblebee APIs require ==encrypted_password== query parameter.

password API returns the encrypted password for your plain text password used for the ALM user. All Bumblebee APIs require ==encrypted_password== query parameter.


## Definition
==HTTP GET== http://hostname[:port]/bumblebee/password?password=plain_password

## Parameters
*	==password:== plain text password which needs to be encrypted

## Examples

	GET http://localhost:8087/bumblebee/password?password=password

## Result Format
``` xml	
<Response>
   <message>fd4OMOXLJjkMR6e64RJh3Q==</message>
</Response>
```