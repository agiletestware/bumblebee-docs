title: API Overview - Bumblebee Documentation
description: Bumblebee provides a set of REST APIs that allow users to easily interact with ALM and integrate ALM into their various frameworks like junit, pytest, ruby, vstest, etc.

API overview
============

Bumblebee provides a set of REST APIs that allow users to easily interact with ALM and integrate ALM into their various frameworks like junit, pytest, ruby, vstest, etc.

Bumblebee also provides client side SDK and on-boarding frameworks (coming soon) that are build on top of these APIs. You can simply add the client library in your code and easily interact with the REST APIs.

If you don't see an API or would like to suggest some improvements, please [contact us](https://www.agiletestware.com/contact-us).
