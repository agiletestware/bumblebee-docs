title: Get test results - Bumblebee Documentation
description: This API method provides the way for getting test information from HP ALM.

# GET /test_results
## Description
This API method provides the way for getting test information from HP ALM.

## Details
**Method:** GET

**Accept:** application/json or application/xml 

**Query parameters:**

| Name               | Description    															| Required 						|
|--------------------|--------------------------------------------------------------------------|-------------------------------|
| url                | URL for HP ALM, must end with "qcbin", e.g. http://almserver:8080/qcbin  | Yes      						|
| user               | User name for HP ALM														| Yes      						|
| encrypted_password | Encrypted password for the user. This is obtained from the /password API | Yes      						|
| domain             | Domain for HP ALM project                                                | Yes      						|
| project            | Project name in HP ALM                                                   | Yes      						|
| testSetPath        | Path to a test set in HP ALM TestLab for which results should be pulled, e.g. Root\\folder\\test set | No (if testPlanPath is set)  |
| testPlanPath		 | Path to a test in HP ALM TestPlan for which results should be pulled, e.g. Subject\\folder\\test	| No (if testSetPath is set)	| 

User must define either testSetPath or testPlanPath parameter in request.

## Results
Method returns different test results depending on what parameters are passed:

*	if **testSetPath** parameter is passed: Bumblebee returns results for all test instances assigned to the passed test set in HP ALM TestLab.
*	if **testPlanPath** parameter is passed: Bumblebee returns results for all tests inside a TestPlan folder (or single test if only one test is specified) in all test sets into which those tests are assigned. See examples below for details.

Results are returned in either XML or JSON format, depending on the value of "Accept" HTTP header. If "Accept" header is not specified Bumblebee returns results in XML format.

### Getting data for a test set in HP ALM TestLab

Getting test results for "Demo" test set in "Root\\Folder1\\Folder2" TestLab folder:

[![Getting results](../img/rest-api/test-results-1.png)](../img/rest-api/test-results-1.png)

**HTTP Request:**

```http
GET /bumblebee/test_results?user=qcuser&amp;url=http://almserver:8080/qcbin&amp;domain=default&amp;project=demo&amp;encrypted_password=fd4OMOXLJjkMR6e64RJh3Q==&amp;testSetPath=Root\Folder1\Folder2\Demo HTTP/1.1
Host: bumblebeeserver:8888
```

**XML Results:**

```xml
<?xml version="1.0" encoding="UTF-8"?>
<results>
    <testSet>
        <testLabPath>Root\Folder1\Folder2\Demo</testLabPath>
        <test>
            <name>SearchForTheFirstAlmIntegrationTool</name>
            <lastRunStatus>Passed</lastRunStatus>
            <lastRunDuration>0</lastRunDuration>
            <lastRunExecutionTime>2018-04-11T12:37:37</lastRunExecutionTime>
            <runsCount>1</runsCount>
            <step>
                <name>Test Result</name>
                <status>Passed</status>
            </step>
        </test>
        <test>
            <name>SomeFailingTest</name>
            <lastRunStatus>Failed</lastRunStatus>
            <lastRunDuration>0</lastRunDuration>
            <lastRunExecutionTime>2018-04-11T12:37:41</lastRunExecutionTime>
            <runsCount>1</runsCount>
            <step>
                <name>Test Result</name>
                <status>Failed</status>
                <actual>Some error occurred</actual>
            </step>
        </test>
        <testsCount>2</testsCount>
    </testSet>
</results>
```

**JSON Results:**

```json
{
    "testSet": [
        {
            "testLabPath": "Root\\Folder1\\Folder2\\Demo",
            "test": [
                {
                    "name": "SearchForTheFirstAlmIntegrationTool",
                    "lastRunStatus": "Passed",
                    "lastRunDuration": 0,
                    "lastRunExecutionTime": "2018-04-11T12:37:37",
                    "runsCount": 1,
                    "step": [
                        {
                            "name": "Test Result",
                            "status": "Passed"
                        }
                    ]
                },
                {
                    "name": "SomeFailingTest",
                    "lastRunStatus": "Failed",
                    "lastRunDuration": 0,
                    "lastRunExecutionTime": "2018-04-11T12:37:41",
                    "runsCount": 1,
                    "step": [
                        {
                            "name": "Test Result",
                            "status": "Failed",
                            "actual": "Some error occurred"
                        }
                    ]
                }
            ],
            "testsCount": 2
        }
    ]
}
```

### Getting results for a test in HP ALM TestPlan

Getting test results for "SomeFailingTest" test in "Subject\\Folder1\\Folder2" TestPlan folder:

[![Getting results](../img/rest-api/test-results-2.png)](../img/rest-api/test-results-2.png)

```http
GET /bumblebee/test_results?user=qcuser&amp;url=http://almserver:8080/qcbin&amp;domain=default&amp;project=demo&amp;encrypted_password=fd4OMOXLJjkMR6e64RJh3Q==&amp;testPlanPath=Subject\Folder1\Folder2\SomeFailingTest HTTP/1.1
Host: bumblebeeserver:8888
```

**XML Results:**

```xml
<?xml version="1.0" encoding="UTF-8"?>
<results>
    <testSet>
        <testLabPath>Root\Folder1\Folder2\Demo</testLabPath>
        <test>
            <name>SomeFailingTest</name>
            <lastRunStatus>Failed</lastRunStatus>
            <lastRunDuration>0</lastRunDuration>
            <lastRunExecutionTime>2018-04-11T12:37:41</lastRunExecutionTime>
            <runsCount>1</runsCount>
            <step>
                <name>Test Result</name>
                <status>Failed</status>
                <actual>Some error occurred</actual>
            </step>
        </test>
        <testsCount>1</testsCount>
    </testSet>
    <testSet>
        <testLabPath>Root\Folder1\Folder2\Another set</testLabPath>
        <test>
            <name>SomeFailingTest</name>
            <lastRunStatus>No Run</lastRunStatus>
            <lastRunDuration>0</lastRunDuration>
            <runsCount>0</runsCount>
            <step>
                <name>Test Result</name>
                <status>No Run</status>
            </step>
        </test>
        <testsCount>1</testsCount>
    </testSet>
</results>
```

**JSON Results:**

```json
{
    "testSet": [
        {
            "testLabPath": "Root\\Folder1\\Folder2\\Demo",
            "test": [
                {
                    "name": "SomeFailingTest",
                    "lastRunStatus": "Failed",
                    "lastRunDuration": 0,
                    "lastRunExecutionTime": "2018-04-11T12:37:41",
                    "runsCount": 1,
                    "step": [
                        {
                            "name": "Test Result",
                            "status": "Failed",
                            "actual": "Some error occurred"
                        }
                    ]
                }
            ],
            "testsCount": 1
        },
        {
            "testLabPath": "Root\\Folder1\\Folder2\\Another set",
            "test": [
                {
                    "name": "SomeFailingTest",
                    "lastRunStatus": "No Run",
                    "lastRunDuration": 0,
                    "runsCount": 0,
                    "step": [
                        {
                            "name": "Test Result",
                            "status": "No Run"
                        }
                    ]
                }
            ],
            "testsCount": 1
        }
    ]
}
```