Overview
========

Bumblebee is a set of tools for integrating various testing frameworks and continuous integration applications with HP ALM.

Customers use Bumblebee to integrate CI applications like Jenkins, Bamboo, and Teamcity with HP ALM.

Customers also use Bumblebee to integrate testing frameworks such as JUnit, testNG, VSTest, NUnit, Pytest, Python, Selenium, Ruby, etc with HP ALM.

Bumblebee is compatible with all versions of HP ALM, Quality Center, and ALM SAAS.

![](img/what_is_bumblebee/hp-alm.png)
![](img/what_is_bumblebee/jenkins.png)
![](img/what_is_bumblebee/bamboo.png)
![](img/what_is_bumblebee/teamcity.jpg)
![](img/what_is_bumblebee/2015-10-15_08h11_52.png)
