Getting Started with bumblebee
==============================

excerpt: "This page will help you get started with bumblebee. You'll be up and running in a jiffy!"
---
This is where you show your users how to set it up. You can use code samples, like this:
[block:code]
{
  "codes": [
    {
      "code": "$http.post('/someUrl', data).success(successCallback);\n\nalert('test');",
      "language": "javascript"
    }
  ]
}
[/block]
Try dragging a block from the right to see how easy it is to add more content!
